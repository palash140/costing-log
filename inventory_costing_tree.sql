# whenever at anytime, any location , a quantity operation is performed over inventory, we want to have  costing for that quantity
# in order to have costing it will depend upon inventeory_costing_transctions, that will be its child
# even child may be outcome of inventory costing transaction -> costing, like in manufacture and recipe

 # so as any inventory quantity logged, we create that inventory costing tree node, that will be used for costing 


 # schema
 # id int : primary key of node 
 # node_id int :  used to represend node_id, it will let us know is it costing transaction or not 
  				# if its costing transaction then it will be referred in inventory_costing_transaction for costing
 # log_id int: to refer location_id, inventory_stock_id, orignal_quantity
 # parent_id int: parent of node, it will be the primary key , that helps to create tree structure for that 
 # transaction_id int: if its costing transaction then it will be id of that else it will be the log operation id 
 # quantity float: if its costing transcation quantity of costing transation else the log quantity
 # costing float: the actual coting of that node
 # in_queue boolean:  check is it waiting for resolved costing or not , only used for costing transaction 


drop table if exists pos_inventory_costing_tree;
create table if not exists pos_inventory_costing_tree(
	id int auto_increment primary key,
	node_type_id int,
	log_id int,
	parent_id int ,
	transaction_id int default null,
	quantity float(10,3),
	costing float(10,3),
	in_queue boolean default false 
);




drop function  if exists createInventoryCostingTreeNode;
delimiter //
create function  createInventoryCostingTreeNode(param_log_id int,param_quantity float,param_node_type_id int,param_parent_id int)
returns int 
begin

	insert into pos_inventory_costing_tree(log_id,quantity,node_type_id,parent_id) 
		values(param_log_id,param_quantity,param_node_type_id,param_parent_id);

	return last_insert_id();

end //
delimiter ;



drop trigger if exists beforeInventoryCosingTreeNodeCreate;
delimiter //
create trigger  beforeInventoryCosingTreeNodeCreate 
before  insert on pos_inventory_costing_tree 
for each row
begin

	declare v_quantity float ;

	# transactions whose costign to be found 
	if NEW.node_type_id = 2 then

		set NEW.in_queue =  1; 

	end if; 


end //

delimiter ;





drop procedure if exists calculateInventoryCostingTreeNodeCosting;
delimiter //
create procedure calculateInventoryCostingTreeNodeCosting(param_location_id int,param_inventory_stock_id int,param_inventory_costing_tree_node_type_id int,param_inventory_costing_tree_node_id int)
begin

	declare v_costing float;
	
	if not exists( select * from pos_inventory_costing_tree where parent_id = param_inventory_costing_tree_node_id and costing is null) then

		select sum(costing) into v_costing from pos_inventory_costing_tree where parent_id = param_inventory_costing_tree_node_id;

		if v_costing is not null then

			 # update first level costing only, means recipe one 
			 update pos_inventory_costing_tree
			 set costing = v_costing 
			 where id = param_inventory_costing_tree_node_id;

			 if param_inventory_costing_tree_node_type_id = 4  then

			 	if isInventoryInCostingTreeQueue(param_location_id,param_inventory_stock_id) then

			 		call dequeueInventoryFromCostingTreeQueue(param_location_id,param_inventory_stock_id);

			 	end if;

			 end if ;


		 end if;



	end if;
	
end //
delimiter ;


drop procedure if exists updateParentCost;
delimiter //
create procedure updateParentCost(param_child_id int)
begin
	
	declare v_parent_id int default null;
	declare v_parent_costing float;
	
	select parent_id into v_parent_id from pos_inventory_costing_tree where id = param_child_id;

	# parent must exists 
	if (v_parent_id is not null) then 

		# none child must be in queue
		if not exists (select * from pos_inventory_costing_tree where parent_id = v_parent_id and costing is null) then

			select sum(costing) into v_parent_costing 
			from pos_inventory_costing_tree where parent_id = v_parent_id	;

			update pos_inventory_costing_tree 
			set costing = v_parent_costing
			where id  = v_parent_id;


		else

			update pos_inventory_costing_tree
			set costing = null 
			where id  = v_parent_id;

		end if;

		call updateParentCost(v_parent_id); 
		
	end if ;


end //

delimiter ;


drop function if exists isInventoryInCostingTreeQueue;
delimiter //
create function isInventoryInCostingTreeQueue(param_location_id int,param_inventory_stock_id int)
returns boolean
begin

	if exists(
		select * 
		from pos_inventory_costing_tree as costtree inner join pos_location_inventory_stock_log as lislog
		on costtree.log_id = lislog.log_id
		where location_id = param_location_id and inventory_stock_id = param_inventory_stock_id
		and in_queue = 1
		limit 1


	) then

		return true;

	else 

		return false;

	end if;


end //

delimiter ;




drop procedure if exists dequeueInventoryFromCostingTreeQueue;
delimiter //
create procedure dequeueInventoryFromCostingTreeQueue(param_location_id int,param_inventory_stock_id int)
begin

	declare v_costing_transaction_id int;
	declare v_costing_consumed_quantity float;
	declare v_costing_queued_quantity float;
	declare v_costing_node_id int;
	declare v_costing_node_type_id int;
	declare v_costing_log_id int;
	declare v_costing_parent_id int;
	declare v_costing float;


	declare v_finished int default 0;
	declare v_queued_inventory cursor for select id,costtree.log_id,parent_id,node_type_id,costtree.quantity 
	from pos_inventory_costing_tree as costtree inner join pos_location_inventory_stock_log as lislog
	on costtree.log_id = lislog.log_id
	where location_id = param_location_id and inventory_stock_id = param_inventory_stock_id
	and in_queue = 1;
	declare continue handler for not found set v_finished = 1;


	open v_queued_inventory;

		queued_inventory_loop: loop

			fetch v_queued_inventory into v_costing_node_id,v_costing_log_id,v_costing_parent_id,v_costing_node_type_id,v_costing_queued_quantity;

			if v_finished = 1 then

				leave queued_inventory_loop ;

			end if;


			if hasInventoryCostingTransaction(param_location_id,param_inventory_stock_id) then

				call calculateInventoryCosting(param_location_id,param_inventory_stock_id,v_costing_queued_quantity,v_costing,v_costing_transaction_id,v_costing_consumed_quantity);

				update pos_inventory_costing_tree 
				set costing = v_costing, transaction_id = v_costing_transaction_id , in_queue = 0, quantity = v_costing_consumed_quantity
				where id = v_costing_node_id;

				SET @@GLOBAL.max_sp_recursion_depth = 255;
				SET @@session.max_sp_recursion_depth = 255; 

				if v_costing_queued_quantity = 0 then

					call updateParentCost(v_costing_node_id);

				else

					# queued the remaining one quantity
					call createInventoryCostingTreeNode(param_location_id,param_inventory_stock_id,v_costing_node_type_id,v_costing_log_id,v_costing_parent_id,null,v_costing_queued_quantity,null,v_costing_node_id);

					leave queued_inventory_loop;

				end if;


			else 

				leave  queued_inventory_loop;

			end if;


		end loop queued_inventory_loop;

	close v_queued_inventory;

end //

delimiter ;




drop function if exists getInventoryCostingTreeNodeParentNode;
delimiter //
create function getInventoryCostingTreeNodeParentNode(param_inventory_costing_tree_node_id int )
returns int
begin

	declare v_inventory_costing_tree_parent_node_id int;

	select  parent_id into v_inventory_costing_tree_parent_node_id
	from pos_inventory_costing_tree 
	where id = param_inventory_costing_tree_node_id;

	return v_inventory_costing_tree_parent_node_id;

end //

delimiter ;



	drop trigger if exists onUpdateOfCostingTree;
delimiter //
create trigger onUpdateOfCostingTree
after update   on pos_inventory_costing_tree
for each row 
begin
	
	declare v_manufacture_node int default 4;
	declare v_location_id int;	
	declare v_inventory_stock_id int;
	declare v_transaction_date datetime;
	declare v_temp int;


	if NEW.node_type_id = 4  then 

	 	if NEW.costing is not null then

			select operation_date,location_id,inventory_stock_id into v_transaction_date,v_location_id,v_inventory_stock_id
			from pos_location_inventory_stock_log 
			where log_id = NEW.log_id;


			select createInventoryCostingTransaction(v_transaction_date,v_location_id,v_inventory_stock_id,v_manufacture_node,NEW.transaction_id,NEW.quantity,NEW.costing/NEW.quantity) into v_temp;

		end if ;


	end if;

end //

delimiter ;



drop event if exists temp;
delimiter //
create event temp
on schedule every 1 minute starts current_timestamp
ON COMPLETION PRESERVE
do 
begin
	
	declare v_count int;

	if exists (select * from pos_global_variables where id = 1 and status = 0 ) then


		update pos_global_variables 
		set status = 1
		where id = 1 ;
		
		insert into testing 
		select null,concat('name',count+1),count+1 from testing order by id desc limit 1; 


	end if;

end //





