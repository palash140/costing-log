-- ##################################### Location Inventory  Stock Log #######################################################################3

drop trigger if exists updateLocationInventoryStockCurrentQuantity;
delimiter //
create trigger updateLocationInventoryStockCurrentQuantity
after insert on pos_location_inventory_stock_log
for each row
begin

	update pos_location_inventory_stock 
	set current_quantity = NEW.current_quantity 
	where location_id = NEW.location_id and inventory_stock_id=NEW.inventory_stock_id;


end //

delimiter ;

drop trigger if exists updateLISOnUpdateLog;
delimiter //
create trigger updateLISOnUpdateLog after update 
on pos_location_inventory_stock_log
for each row 
begin 

	update pos_location_inventory_stock 
	set current_quantity = NEW.current_quantity 
	where location_id = NEW.location_id and inventory_stock_id=NEW.inventory_stock_id;

end //

-- ##################################### Location Inventory  Stock Log #######################################################################3

delimiter ;



-- ##################################### Wastage #######################################################################3

drop trigger if exists `at_wastage_create` ;
delimiter //
create trigger `at_wastage_create`
after insert on `pos_location_inventory_stock_wastage`
for each row
begin
	
    call logLocationInventoryStock(NEW.location_id,NEW.inventory_stock_id,NEW.wastage_quantity,5,NEW.wastage_id,null,NEW.wastage_date);


	if NEW.wastage_date < current_date() then 

		call readjustLogByDate(NEW.location_id,NEW.inventory_stock_id,NEW.wastage_date);

	end if;



end //



drop trigger if exists `at_wastage_update` //
create trigger `at_wastage_update`
after update on `pos_location_inventory_stock_wastage`
for each row
begin
	

    call logLocationInventoryStock(OLD.location_id,OLD.inventory_stock_id,OLD.wastage_quantity,17,OLD.wastage_id,null,NEW.wastage_date);

    call logLocationInventoryStock(NEW.location_id,NEW.inventory_stock_id,NEW.wastage_quantity,5,NEW.wastage_id,null,NEW.wastage_date);

    if NEW.wastage_date < current_date() then 

		call readjustLogByDate(NEW.location_id,NEW.inventory_stock_id,NEW.wastage_date);

	end if;

end //


drop trigger if exists `at_wastage_delete` //
create trigger `at_wastage_delete`
after delete  on `pos_location_inventory_stock_wastage`
for each row
begin
	
    call logLocationInventoryStock(OLD.location_id,OLD.inventory_stock_id,OLD.wastage_quantity,17,OLD.wastage_id,null,OLD.wastage_date);

    if OLD.wastage_date < current_date() then 

		call readjustLogByDate(OLD.location_id,OLD.inventory_stock_id,OLD.wastage_date);

	end if;
	
end //

-- ##################################### Wastage #######################################################################3

delimiter ;

-- ##################################### Purchaes Order #######################################################################3

drop trigger if exists at_purchase_order_delivered;
delimiter // 

create trigger at_purchase_order_delivered after update
 on pos_purchase_orders_new
 for each row
 begin

 	if OLD.purchase_order_status_id != NEW.purchase_order_status_id then 

 		if NEW.purchase_order_status_id = 4 then 
 			call logDeliveredPurchaseOrder(NEW.location_id,NEW.purchase_order_id);
 		end if ;

 	end if ;
end //



delimiter ;

drop trigger if exists at_puruchase_order_transaction_delete;
delimiter //
create trigger at_puruchase_order_transaction_delete 
after delete on `pos_purchase_order_transactions_new`
for each row
begin 

	declare v_location_id int;
	declare v_purchase_order_date datetime;

	# operation_date also campture seconds 
	declare v_log_operation_date datetime ; 




	select location_id,purchase_order_date into v_location_id,v_purchase_order_date from pos_purchase_orders_new where purchase_order_id = OLD.purchase_order_id;

	select operation_date into v_log_operation_date 
	from pos_location_inventory_stock_log
	where location_id=v_location_id and inventory_stock_id = OLD.inventory_stock_id and operation_type_id = 2 and inventory_operation_id = OLD.purchase_order_transaction_id;
	

	# make the log entry  at same date with just after 1 second as revert 
	call logLocationInventoryStock(v_location_id,OLD.inventory_stock_id,OLD.purchase_qty,16,OLD.purchase_order_transaction_id,null,DATE_ADD(v_log_operation_date, INTERVAL 1 SECOND));

	# readjust the log
	if v_purchase_order_date < current_date() then 

		call readjustLogByDate(v_location_id,OLD.inventory_stock_id,v_purchase_order_date);

	end if;


	call updateInventoryAvailableRatePerUnit(v_location_id,OLD.inventory_stock_id);


end //

delimiter ;

drop trigger if exists at_puruchase_order_transaction_update;
delimiter //
create trigger at_puruchase_order_transaction_update 
after update on `pos_purchase_order_transactions_new`
for each row
begin 


	declare v_last_purchase_qty float;
	declare v_location_id int;
	declare v_purhcase_order_date datetime;


	if OLD.purchase_qty != NEW.purchase_qty then
		-- revert last log of this purchase	

		select location_id,purchase_order_date into v_location_id,v_purhcase_order_date from pos_purchase_orders_new where purchase_order_id = NEW.purchase_order_id;

		-- last qty minus
		call logLocationInventoryStock(v_location_id,NEW.inventory_stock_id,OLD.purchase_qty,16,NEW.purchase_order_transaction_id,null,v_purhcase_order_date);

		-- update to new qty 
		call logLocationInventoryStock(v_location_id,NEW.inventory_stock_id,NEW.purchase_qty,2,NEW.purchase_order_transaction_id,null,v_purhcase_order_date);


		if v_purhcase_order_date < current_date() then 

			call readjustLogByDate(v_location_id,NEW.inventory_stock_id,v_purhcase_order_date);

		end if;




	end if;	


	if OLD.purchase_qty != NEW.purchase_qty || OLD.purchase_rate != NEW.purchase_rate then

		call updateInventoryAvailableRatePerUnit(v_location_id,NEW.inventory_stock_id);

	end if;

end //

-- ##################################### Purchaes Order #######################################################################3

delimiter ;


-- ####################################### Manufacture #############################################

drop trigger if exists logUpdateManufactureStock;
delimiter //
create trigger logUpdateManufactureStock
after update on pos_manufacture 
for each row
begin

	
	IF NEW.manufacturing_quantity != OLD.manufacturing_quantity  then

 		-- 12  manufactur revert minus
 		-- 10 sale manufacture revert
	 	call logLocationInventoryStock(OLD.location_id,OLD.inventory_stock_id,OLD.manufacturing_quantity,
	 	IF(OLD.manufacture_type_id = 1,12,10),
	 	OLD.manufacture_id,null,NEW.manufacturing_date);

	 	-- 13 manufacture consumptions revert plus
	 	-- 11 sale manufacture consumptions revert
	 	call logItemRecipes(OLD.location_id,OLD.inventory_stock_id,OLD.manufacturing_quantity,
	 	IF(OLD.manufacture_type_id = 1,13,11),
	 	OLD.manufacture_id,NEW.manufacturing_date);


	 	-- 4 manufacture plus
	 	-- 7 sale manufacture plus
	 	call logLocationInventoryStock(NEW.location_id,NEW.inventory_stock_id,NEW.manufacturing_quantity,
	 	IF(OLD.manufacture_type_id = 1,4,7),
	 	NEW.manufacture_id,null,NEW.manufacturing_date);



	 	-- 6 manufacture consumptions minus
	 	-- 8 sales manufacture consumptions
	 	call logItemRecipes(NEW.location_id,NEW.inventory_stock_id,NEW.manufacturing_quantity,
	 	IF(OLD.manufacture_type_id = 1,6,8),
	 	NEW.manufacture_id,NEW.manufacturing_date);

	end if ;


end //

delimiter ;



drop trigger if exists logLocationInventoryStockManufacture;
delimiter //
CREATE TRIGGER `logLocationInventoryStockManufacture` AFTER INSERT ON `pos_manufacture`
 FOR EACH ROW 
 begin


	-- 4 manufacture plus
	-- 7 sales manufacture plus
 	call logLocationInventoryStock(NEW.location_id,NEW.inventory_stock_id,NEW.manufacturing_quantity,
 	IF(NEW.manufacture_type_id = 1 ,4,7),
 	NEW.manufacture_id,null,NEW.manufacturing_date);



 	-- 6 manufacture consumptions minus
 	-- 8 sales manufacture consumption
 	call logItemRecipes(NEW.location_id,NEW.inventory_stock_id,NEW.manufacturing_quantity,
 	IF(NEW.manufacture_type_id = 1 ,6,8),
 	NEW.manufacture_id,NEW.manufacturing_date);


  end   //
  
delimiter ;



drop trigger if exists logDeleteManufactureStock;
delimiter //
create trigger logDeleteManufactureStock
before delete on pos_manufacture 
for each row
begin 

	-- 12  manufactur revert minus
	-- 10 sale manufacture revert minus
 	call logLocationInventoryStock(OLD.location_id,OLD.inventory_stock_id,OLD.manufacturing_quantity,
 	IF(OLD.manufacture_type_id = 1,12,10),
 	OLD.manufacture_id,null,OLD.manufacturing_date);


 	-- 13 manufacture consumptions revert plus
 	-- 11 sale manufacture consumptions revert plus
 	call logItemRecipes(OLD.location_id,OLD.inventory_stock_id,OLD.manufacturing_quantity,
 	IF(OLD.manufacture_type_id = 1,13,11),
 	OLD.manufacture_id,OLD.manufacturing_date);


end // 


-- ############################################# Manufacture #######################################

delimiter ;


-- ############################################# Sales Order #######################################

drop trigger if exists AtSalesOrderUpdate;
delimiter //
CREATE trigger AtSalesOrderUpdate
after update on `pos_sales_order`
for each row 
begin 
	
  	IF OLD.sales_order_status_id!=NEW.sales_order_status_id then
	
		if   NEW.sales_order_status_id = 5 then

			call cancelSalesOrder(NEW.sales_order_id,NEW.location_id,NEW.company_id,NEW.floor_id,NEW.employee_id);

         end if;


         -- sales order status changed to ordered 
    	if NEW.sales_order_status_id = 4  THEN

    		CALL logDeliveredSalesOrderTransactions(NEW.location_id,NEW.sales_order_id,NEW.sales_order_type_id);

    	end if;


    	if OLD.sales_order_status_id = 4 and NEW.sales_order_status_id = 2 then

	  		call logDeliverToProgressSalesOrderTransactions(NEW.location_id,NEW.sales_order_id,NEW.sales_order_type_id);
  	
		 end if;

	  
	END IF;

end //

-- ############################################ Sales Order ############################################
