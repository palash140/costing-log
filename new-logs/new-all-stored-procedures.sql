-- ########################################## purchase ###################################################
drop procedure if exists logDeliveredPurchaseOrder;
delimiter //
create procedure logDeliveredPurchaseOrder(location_id int,purchase_order_id int)
begin
	
	declare v_purchase_order_transaction_id int;
	declare v_inventory_stock_id int;
	declare v_purchase_qty float;
	declare v_purchase_order_transactions_finished int default 0;
	declare v_purhcase_order_date datetime;


	-- find all the purchase order transaction, id, item, quty 
	declare v_purchase_order_transactions cursor for	
	select inventory_stock_id,purchase_order_transaction_id,purchase_qty 
	from pos_purchase_order_transactions_new 
	where pos_purchase_order_transactions_new.purchase_order_id = purchase_order_id;
	declare continue handler for not found set v_purchase_order_transactions_finished  = 1;


	select purchase_order_date into v_purhcase_order_date 
	from pos_purchase_orders_new 
	where pos_purchase_orders_new.purchase_order_id = purchase_order_id;


	open v_purchase_order_transactions;

		purchase_order_transactions_loop: loop

			fetch v_purchase_order_transactions 	
			into v_inventory_stock_id,v_purchase_order_transaction_id,v_purchase_qty;

			if v_purchase_order_transactions_finished = 1 then

				leave purchase_order_transactions_loop;

			end if;

			call logLocationInventoryStock(location_id,v_inventory_stock_id,v_purchase_qty,2,v_purchase_order_transaction_id,null,v_purhcase_order_date);

			if v_purhcase_order_date < current_date() then 

				call readjustLogByDate(location_id,v_inventory_stock_id,v_purhcase_order_date);

			end if;

			call updateInventoryAvailableRatePerUnit(location_id,v_inventory_stock_id);


		end loop purchase_order_transactions_loop;

	close v_purchase_order_transactions;


end //

-- ################################ Purchase #####################################

delimiter ;

-- ################################ Log ##############################################

drop procedure if exists logLocationInventoryStock;
delimiter //
create procedure logLocationInventoryStock(location_id int,inventory_stock_id int,quantity float,operation_type_id int,inventory_operation_id int,param_other_info text,param_operation_date datetime)

begin

	declare v_current_quantity float;
	declare v_opening_quantity float;
	declare v_last_current_quantity float; 

	declare v_operation varchar(100);
	declare v_new_current_quantity float;
	declare v_count_temp int;



	
	select current_quantity,opening_quantity 
	into v_current_quantity,v_opening_quantity
	from pos_location_inventory_stock as lis
	where lis.location_id = location_id and lis.inventory_stock_id = inventory_stock_id  
	limit 1;


	if v_current_quantity is null  then

		if v_opening_quantity is null then

			set v_last_current_quantity = 0;

		else 

			set v_last_current_quantity = v_opening_quantity; 

		end if;

	else 

		set v_last_current_quantity = v_current_quantity;

	end if;



	-- fetch math operation over quantity based on the operation_type_id
	select pos_log_operation_types.operation  into v_operation
	from pos_log_operation_types 
	where pos_log_operation_types.log_operation_type_id = operation_type_id;


	if v_operation = 'plus' then

		set v_new_current_quantity = v_last_current_quantity + quantity;

	end if;

	if v_operation = 'minus' then 

		set v_new_current_quantity = v_last_current_quantity - abs(quantity); 

	end if ;


	insert into 
	pos_location_inventory_stock_log(`location_id`,`inventory_stock_id`,`quantity`,`current_quantity`,`operation_type_id`,`inventory_operation_id`,`other_info`,`operation_date`)
	values(location_id,inventory_stock_id,quantity,v_new_current_quantity,operation_type_id,inventory_operation_id,param_other_info,param_operation_date);



end //

delimiter ;

drop procedure if exists readjustLogByDate;
delimiter //

create procedure readjustLogByDate(location_id int,inventory_stock_id int,readjustDate datetime)
begin 


	declare v_day_before_current_quantity float;
	declare v_current_quantity float;
	declare v_inventory_logs_finished int default 0;

	declare v_log_id int;
	declare v_quantity float;
	declare v_operation_type_id int;
	declare v_operation varchar(100);


	-- iterate over the quantity from the adjustment date
	declare v_inventory_logs cursor for 
	select log_id,quantity,operation_type_id  from pos_location_inventory_stock_log as poslog
	where  poslog.location_id = location_id and poslog.inventory_stock_id = inventory_stock_id and operation_date >= readjustDate
	order by operation_date asc ;
	declare continue handler for not found set v_inventory_logs_finished = 1 ;


	-- first get day before last current quantity
	select current_quantity into v_day_before_current_quantity
	from pos_location_inventory_stock_log  as poslog 
	where poslog.location_id = location_id  and poslog.inventory_stock_id = inventory_stock_id and operation_date < readjustDate   
	order by operation_date desc limit 1;


	-- update the current quantity in lis
	update pos_location_inventory_stock  as lis 
	set lis.current_quantity = v_day_before_current_quantity 
	where lis.location_id = location_id and lis.inventory_stock_id = inventory_stock_id ;


	set v_current_quantity = v_day_before_current_quantity ;

	open v_inventory_logs;

		inventory_logs_loop: loop	

			fetch v_inventory_logs 
			into v_log_id,v_quantity,v_operation_type_id;


			if v_inventory_logs_finished = 1 then
			
				leave inventory_logs_loop ; 

			end if ;	

			-- fetch math operation over quantity based on the operation_type_id
			select pos_log_operation_types.operation  into v_operation
			from pos_log_operation_types 
			where pos_log_operation_types.log_operation_type_id = v_operation_type_id;


			if v_operation = 'plus' then

				set v_current_quantity = v_current_quantity + v_quantity;

			end if;

			if v_operation = 'minus' then 

				set v_current_quantity = v_current_quantity -  v_quantity;

			end if ;


			update pos_location_inventory_stock_log	 as poslog
			set poslog.current_quantity = v_current_quantity
			where poslog.log_id = v_log_id;


		end loop inventory_logs_loop;

	close v_inventory_logs;

end //	


-- ########################################################## log ###################################################

delimiter ;

-- ########################################################## Others ################################################



drop procedure if exists logItemRecipes;
delimiter //
create procedure logItemRecipes(param_location_id int,param_inventory_stock_id int,param_quantity float,param_recipe_operation int,param_inventory_operation_id int,param_operation_date datetime)
begin

	declare v_raw_product_id int;
	declare recipe_quantity float;
	declare recipes_finished int default 0;
	declare log_quantity float; 
	declare recipe_consumed_quantity float;

	declare v_unit_id int;

	declare v_wastage_id int default null;


	declare v_revert_operation_type_id int;


	declare v_lis_wastage_percentage_id int default null;


	declare v_last_log_id int;
	-- fetch current item recipie item and the quantity participation
	declare recipes cursor for 
	select raw_product_id,qty,unit_id
	from pos_recipe_new 
	where final_product_id = param_inventory_stock_id;
	declare continue handler for not found set recipes_finished = 1; 

	-- initalize recipes 
	open recipes;

		-- loop over recipe
		recipes_loop: LOOP

			-- fetch recipe item(raw_product_id) id and quantity
			fetch recipes into v_raw_product_id,recipe_quantity,v_unit_id;

			-- is it last recipe item
			if recipes_finished = 1 then

				leave recipes_loop;

			end if;

			-- 13 revert / delete because of normal manufacture
			-- 11 revert / delete becaues of sale manufacture
			if param_recipe_operation in (13,11) then

			    -- 11 sale manufacture consumption revert
				-- 8 sale manufacture consumption
			    if param_recipe_operation = 11 then
					set v_revert_operation_type_id = 8;
				end if;

				-- 13 manufacture consumption revert
				-- 6 manufacture conusmption
				if param_recipe_operation = 13 then
					set v_revert_operation_type_id = 6;
				end if;
				

				-- its entry in log
				select quantity,other_info into log_quantity,v_lis_wastage_percentage_id
				from pos_location_inventory_stock_log
				where pos_location_inventory_stock_log.location_id = param_location_id and
				pos_location_inventory_stock_log.inventory_stock_id = v_raw_product_id and
			    pos_location_inventory_stock_log.operation_type_id=v_revert_operation_type_id and 
			    pos_location_inventory_stock_log.inventory_operation_id = param_inventory_operation_id
			    order by created_at desc
			    limit 1;


			    set recipe_consumed_quantity = log_quantity;


				-- revert the wastage percentag ie exists 
				if v_lis_wastage_percentage_id is not null then

					delete from pos_location_inventory_stock_wastage where wastage_id = v_lis_wastage_percentage_id;

				end if;



				 call logLocationInventoryStock(param_location_id,v_raw_product_id,recipe_consumed_quantity,param_recipe_operation,param_inventory_operation_id,null,param_operation_date);

			 else 



			 	select param_quantity * recipe_quantity into recipe_consumed_quantity;

				call logLocationInventoryStock(param_location_id,v_raw_product_id,recipe_consumed_quantity,param_recipe_operation,param_inventory_operation_id,null,param_operation_date);

				set v_last_log_id = last_insert_id();

				-- check recipe exists or not
				if exists(select * from pos_inventory_stock_new   where pos_inventory_stock_new.inventory_stock_id= v_raw_product_id and wastage_percentage > 0)
				then

	--				call logWastagePercentage(param_location_id,v_raw_product_id,v_unit_id,param_recipe_operation,recipe_consumed_quantity,param_inventory_operation_id,v_wastage_id);
					call logWastagePercentage(param_location_id,v_raw_product_id,recipe_consumed_quantity,param_recipe_operation,param_inventory_operation_id,v_unit_id,v_wastage_id);

					if v_wastage_id is not null then

						update pos_location_inventory_stock_log 
					    set other_info = v_wastage_id
					    where log_id=  v_last_log_id;

					end if;

				end if;




			end if;







		end loop recipes_loop;

	close recipes;	

end //



-- ########################################################## Others ################################################ ;


delimiter ;

-- ########################################################## Sales Order ############################################# 


drop procedure if exists cancelSalesOrder;
delimiter //
create procedure cancelSalesOrder(sales_order_id_param int,location_id_param int,company_id_param int,floor_id_param int,employee_id_param int)
begin

	declare v_inventory_stock_id int;
	declare v_sales_qty float;
	declare v_is_consumed int default 0;
	declare v_current_quantity float ;
	declare v_required_manufacture_quantity float;
	declare v_unit_id int;
	declare v_reason varchar(500);



	declare v_is_transactions_finished int default 0;
	declare v_transactions cursor for 
	select inventory_stock_id,sales_qty,unit_id,is_consumed 
	from pos_sales_order_transaction
	where sales_order_id=sales_order_id_param;
	declare continue handler for not found set v_is_transactions_finished = 1;




	open v_transactions;

		tranactions_loop: LOOP

			fetch v_transactions
			into v_inventory_stock_id,v_sales_qty,v_unit_id,v_is_consumed;

			if v_is_transactions_finished then
				leave tranactions_loop;
			end if;

			if v_is_consumed then

				select current_quantity into v_current_quantity
				 from pos_location_inventory_stock as lis
				where lis.location_id = location_id_param and lis.inventory_stock_id = v_inventory_stock_id;


				-- do we have sales quantity more than the current/avaliable quantity 
			  if ( v_sales_qty > v_current_quantity)  then

				-- get the required quantity to be manufacture for item
				select (v_sales_qty - v_current_quantity) into v_required_manufacture_quantity;

				 -- manufacture it
				 insert into pos_manufacture (company_id,location_id,floor_id,employee_id,inventory_stock_id,manufacturing_quantity,unit_id,manufacture_type_id,extra_info)
				 values(company_id_param,location_id_param,floor_id_param,employee_id_param,v_inventory_stock_id,v_required_manufacture_quantity,v_unit_id,2,'');

			end if;

				select concat("{'sales_order_id':",sales_order_id_param,"}") into v_reason;

			 -- waste it
				insert into pos_location_inventory_stock_wastage  
					(company_id,location_id,inventory_stock_id,wastage_date,wastage_quantity,reason,unit_id,employee_id,parent_id,wastage_type_id)
				 values(company_id_param,location_id_param,v_inventory_stock_id,now(),v_sales_qty,v_reason,v_unit_id,employee_id_param,null,1);

		   end if;


		end loop tranactions_loop;

	close v_transactions;


end //


delimiter ;

DROP PROCEDURE IF EXISTS logDeliveredSalesOrderTransactions;
DELIMITER //
CREATE PROCEDURE logDeliveredSalesOrderTransactions(location_id_param int,sales_order_id_param int,sales_availability_id_param int)
BEGIN
  

DECLARE v_sales_order_transaction_id int;
DECLARE v_location_id int;
DECLARE v_inventory_stock_id int;
DECLARE v_location_inventory_stock_id int;
DECLARE v_current_quantity float;
DECLARE v_sales_qty float ;
DECLARE v_new_current_quantity float ;

DECLARE v_finished int default 0;
DECLARE v_has_manufactured int ;

DECLARE v_manufactured_quantity float;

DECLARE v_company_id int;
DECLARE v_floor_id int;
DECLARE v_employee_id int;
DECLARE v_unit_id int ;
DECLARE v_manufacture_id int;

DECLARE v_other_info text default null;

DECLARE v_required_manufacture_quantity float;


declare v_sales_order_date datetime;




-- declare the transaction to be processed
DECLARE v_transactions CURSOR for 
	select inventory_stock_id,sales_order_transaction_id,sales_qty,unit_id
	from 
	pos_sales_order_transaction 
  where sales_order_id =sales_order_id_param;
-- handler for transaction loop, when reach to end 
-- runs when loop each at end of item
DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_finished = 1;




select sales_order_date into v_sales_order_date
from pos_sales_order 
where sales_order_id = sales_order_id_param;


-- fetch passed sales order company, floor and employee id 
select company_id,floor_id,employee_id  into v_company_id,v_floor_id,v_employee_id
from pos_sales_order where sales_order_id = sales_order_id_param;


-- initalize the transactiosn 
open v_transactions;

    -- loop through each transaction
   read_loop: LOOP

    -- fetch current loop index, transaction details 
    -- fetch transcation, item over which transaction performed, with quantity and unit, along with transaction_id
     fetch v_transactions
     into v_inventory_stock_id,v_sales_order_transaction_id,v_sales_qty,v_unit_id;


     -- check is last item fetched
     if v_finished = 1 then

        -- exit the loop
        leave read_loop ;

     end if; 


      -- check current transaction item, manufacturing flag
      -- passed location, get item manufacturing detail
      -- get location ->  sales order  in inventory stocks get transaction item details, and fetch only manufacturing flag and its current quantity detail 
      select has_manufacturing,current_quantity into v_has_manufactured,v_current_quantity
      from pos_location_inventory_stock 
      where pos_location_inventory_stock.location_id = location_id_param and inventory_stock_id = v_inventory_stock_id;


        if v_has_manufactured  = 0 then 

          -- 3 sale minus
          -- the item is used directly, deduct its quantity, log it and  update its current quantity
         call logLocationInventoryStock(location_id_param,v_inventory_stock_id,v_sales_qty,3,v_sales_order_transaction_id,v_other_info,v_sales_order_date);

        else

         set v_other_info = null;

         -- do we have sales quantity more than the current/avaliable quantity 
          if ( v_sales_qty > v_current_quantity)  then

            -- get the required quantity to be manufacture for item
            select (v_sales_qty - v_current_quantity) into v_required_manufacture_quantity;

            -- manufacture the required quantity
            -- 2 means sales type manufacture
            insert into pos_manufacture (company_id,location_id,floor_id,employee_id,inventory_stock_id,manufacturing_quantity,unit_id,manufacture_type_id,extra_info,manufacturing_date)
            values(v_company_id,location_id_param,v_floor_id,v_employee_id,v_inventory_stock_id, v_required_manufacture_quantity,v_unit_id,2,'',CURRENT_TIMESTAMP());

            -- the manufacture will update the current quantity of stock also 

            set v_other_info= LAST_INSERT_ID();

          end if ;

            -- 3 sale minus
            -- now calculate the currrent quantity , reduce the required quantity from current quantity, 
            -- update the current quantity
            call logLocationInventoryStock(location_id_param,v_inventory_stock_id,v_sales_qty,3,v_sales_order_transaction_id,v_other_info,v_sales_order_date);

        end if;


       call logTransactionVariationSelectionRecipe(location_id_param,v_inventory_stock_id,v_sales_order_transaction_id,v_sales_qty,3);

       call logTransactionSalesAvailabilityRecipe(location_id_param,v_inventory_stock_id,v_sales_order_transaction_id,sales_availability_id_param,v_sales_qty,3);


   end loop read_loop;
     

close v_transactions;

  
END //


delimiter ;

DROP PROCEDURE IF EXISTS logTransactionVariationSelectionRecipe;
DELIMITER //
CREATE PROCEDURE logTransactionVariationSelectionRecipe(location_id_param int,inventory_stock_id_param int,sales_order_transaction_id_param int,sales_qty_param float,param_operation_id int)
BEGIN

   DECLARE v_selection_id int ;
   DECLARE v_inventory_stock_id int;
   DECLARE v_selection_recipe_qty float default 0;
   DECLARE v_selected_recipes_finished int default 0;
   DECLARE  v_log_quantity float;
   DECLARE v_sales_order_date datetime;
   DECLARE v_sales_order_id int;

  declare recipe_consumed_quantity float;

   declare v_variation_selection_recipes cursor for 
   select raw_product_id,final_product_id,qty  
   from pos_selection_recipe where final_product_id 
   in(select tranvarsel.selection_id  
      from pos_sales_order_transaction_variations as tranvar
      join  
      pos_sales_order_transaction_variation_selections as tranvarsel
      on tranvar.sales_order_transaction_variation_id = tranvarsel.sales_order_transaction_variation_id
      where tranvarsel.selected = 1  and tranvar.sales_order_transaction_id = sales_order_transaction_id_param
   );

   declare continue handler for not found set v_selected_recipes_finished = 1 ;



   select sales_order_id into v_sales_order_id from pos_sales_order_transaction where sales_order_transaction_id = sales_order_transaction_id_param;

   select sales_order_date into v_sales_order_date from pos_sales_order where sales_order_id = v_sales_order_id;

   open v_variation_selection_recipes;

      recipe_loop: LOOP

        fetch v_variation_selection_recipes into v_inventory_stock_id,v_selection_id,v_selection_recipe_qty;

        if v_selected_recipes_finished = 1 then

          leave recipe_loop;

        end if;

        set recipe_consumed_quantity = sales_qty_param * v_selection_recipe_qty;
        -- revert 
        if param_operation_id = 9  then

          -- fetch the quantity from log
          select quantity into v_log_quantity
          from pos_location_inventory_stock_log
         where location_id = location_id_param and
          inventory_stock_id = v_inventory_stock_id and
          operation_type_id=3
          and inventory_operation_id = sales_order_transaction_id_param
          order by created_at desc
          limit 1;

          if (v_log_quantity is not null) then 

            set recipe_consumed_quantity = v_log_quantity;

          end if;

        end if;

        -- 3 sale minus
       call logLocationInventoryStock(location_id_param,v_inventory_stock_id,recipe_consumed_quantity,param_operation_id,sales_order_transaction_id_param,null,v_sales_order_date);

      end loop recipe_loop;

   close v_variation_selection_recipes;

END //

delimiter ;



drop procedure if exists logTransactionSalesAvailabilityRecipe;
delimiter //
create procedure logTransactionSalesAvailabilityRecipe(location_id_param int,inventory_stock_id_param int,sales_order_transaction_id_param int, sales_availability_id_param int, sales_qty_param float,param_operation_id int)
begin
     declare v_sales_availability_recipe_finished int default 0;
     declare v_inventory_stock_id int;
     declare v_sales_availability_recipe_qty float;
     declare v_log_quantity float;
     declare recipe_consumed_quantity float;

     DECLARE v_sales_order_date datetime;
     DECLARE v_sales_order_id int;



     declare v_transaction_sales_availability_recipes cursor for  
     select raw_product_id,qty from pos_sales_availiability_recipes 
     where final_product_id in (
      select stock_sales_availiability_id 
      from  pos_stock_sales_availiability
      where location_inventory_stock_id in (
        select location_inventory_stock_id  
     from pos_location_inventory_stock 
     where location_id=location_id_param and inventory_stock_id=inventory_stock_id_param 
      )
      and sales_availiability_id = sales_availability_id_param
     );


     declare continue  handler for not found set v_sales_availability_recipe_finished = 1;


   select sales_order_id into v_sales_order_id from pos_sales_order_transaction where sales_order_transaction_id = sales_order_transaction_id_param;

   select sales_order_date into v_sales_order_date from pos_sales_order where sales_order_id = v_sales_order_id;


     open v_transaction_sales_availability_recipes;

        recipe_loop: LOOP 

          fetch v_transaction_sales_availability_recipes into v_inventory_stock_id,v_sales_availability_recipe_qty;

          if v_sales_availability_recipe_finished = 1 then

            leave recipe_loop;

          end if;

          set recipe_consumed_quantity = sales_qty_param * v_sales_availability_recipe_qty;
          -- revert 
          if param_operation_id = 9  then

            -- fetch the quantity from log
            select quantity into v_log_quantity
            from pos_location_inventory_stock_log
           where location_id = location_id_param and
            inventory_stock_id = v_inventory_stock_id and
            operation_type_id=3
            and inventory_operation_id = sales_order_transaction_id_param
            order by created_at desc
            limit 1;

            if (v_log_quantity is not null) then 

              set recipe_consumed_quantity = v_log_quantity;

            end if;


          end if;


          -- 3 sale minus
          call logLocationInventoryStock(location_id_param,v_inventory_stock_id,recipe_consumed_quantity,param_operation_id,sales_order_transaction_id_param,null,v_sales_order_date);

        end loop recipe_loop;

     close v_transaction_sales_availability_recipes;

end //

delimiter ;


drop procedure if exists logDeliverToProgressSalesOrderTransactions;
delimiter //

create procedure logDeliverToProgressSalesOrderTransactions(location_id int,sales_order_id int,sales_availability_id_param int)
begin 
	
	declare v_inventory_stock_id int;
	declare v_sales_order_transaction_id int;
	declare v_sales_qty float;
	declare v_sales_order_transactions_finished int default 0;
	declare v_has_manufactured int default 0;


	 DECLARE v_sales_order_date datetime;

	declare v_manufacture_id int;
	
	-- find all the trasaction of sales order
	declare v_sales_order_transactions cursor for 
	select inventory_stock_id,sales_order_transaction_id,sales_qty
	from pos_sales_order_transaction
	where pos_sales_order_transaction.sales_order_id = sales_order_id;
	declare continue handler for not found set v_sales_order_transactions_finished = 1;


	select sales_order_date into v_sales_order_date from pos_sales_order where pos_sales_order.sales_order_id = sales_order_id;


	-- first revert or restore all the values 
	open v_sales_order_transactions;

		sales_order_transactions_loop: loop

			fetch v_sales_order_transactions 	
			into v_inventory_stock_id,v_sales_order_transaction_id,v_sales_qty;

			if v_sales_order_transactions_finished = 1 then

				leave sales_order_transactions_loop;

			end if;

				-- 9 sales revert
			call logLocationInventoryStock(location_id,v_inventory_stock_id,v_sales_qty,9,v_sales_order_transaction_id,null,v_sales_order_date);

			call logTransactionVariationSelectionRecipe(location_id,v_inventory_stock_id,v_sales_order_transaction_id,v_sales_qty,9);

       		call logTransactionSalesAvailabilityRecipe(location_id,v_inventory_stock_id,v_sales_order_transaction_id,sales_availability_id_param,v_sales_qty,9);


		end loop sales_order_transactions_loop;

	close v_sales_order_transactions;


   set v_sales_order_transactions_finished = 0;


	-- delete the manufacturerd, if manufacture for the time of transaction
	open v_sales_order_transactions;

		sales_order_transactions_loop: loop

			fetch v_sales_order_transactions 	
			into v_inventory_stock_id,v_sales_order_transaction_id,v_sales_qty;

			if v_sales_order_transactions_finished = 1 then

				leave sales_order_transactions_loop;

			end if;

			select has_manufacturing into v_has_manufactured 
			from pos_location_inventory_stock 
			where pos_location_inventory_stock.location_id = location_id and inventory_stock_id = v_inventory_stock_id;


			-- becauase manfuacture item will be deleted  here by api

			if v_has_manufactured  = 1 then 

				-- check is it manufactured
				-- last log of sales of this transaction , at location,inventory_stock_id
				select other_info  into  v_manufacture_id
				from pos_location_inventory_stock_log as log
				where log.location_id = location_id and log.inventory_stock_id= v_inventory_stock_id and operation_type_id = 3 
				and inventory_operation_id = v_sales_order_transaction_id
				order by created_at desc limit 1;


				if (v_manufacture_id is not null)	 then 

					-- manufacture delete	
					delete from pos_manufacture where manufacture_id = v_manufacture_id;

				end if;

			end if;


		end loop sales_order_transactions_loop;

	close v_sales_order_transactions;


end //



-- ########################################################## Sales Order ############################################# 


delimiter ;


-- ######################################################### Wastage percentage #############################################3



drop procedure if exists logWastagePercentage;
delimiter //
create procedure logWastagePercentage(param_location_id int,param_inventory_stock_id int,param_consumed_quantity float,param_operation_type_id int,param_inventory_operation_id int,param_unit_id int,out param_wastage_id int)

begin

	declare v_wastage_percentage float default 0 ;
	declare v_last_log_operation varchar(100); 
	declare v_company_id int;

	declare v_total_consumed_quantity float;
	declare v_wastage_qty float;

	declare v_reason text;


	select cast(wastage_percentage as decimal(10,2)) into v_wastage_percentage
	from pos_inventory_stock_new 
	where pos_inventory_stock_new.inventory_stock_id = param_inventory_stock_id;

	if v_wastage_percentage  > 0  then


		select ((100 * param_consumed_quantity)/(100- v_wastage_percentage)) into v_total_consumed_quantity;
		set v_wastage_qty= v_total_consumed_quantity - param_consumed_quantity; 




		-- 6 normal manufacture consumption,
		-- 8 sale manufacture consumpktion
		if param_operation_type_id in (6,8)  then

			select company_id into v_company_id from pos_location where pos_location.location_id = param_location_id;

			-- 4 wastage type, means beacuase of manufcature comsumpttion

			select concat("{'manufcature_id':",param_inventory_operation_id,"}") into v_reason;

			-- insert the entry as wastage 
			-- 4 waste because of manufacture consumption
	       insert into 
	       	pos_location_inventory_stock_wastage (company_id,parent_id,employee_id,location_id,inventory_stock_id,wastage_date,wastage_quantity,unit_id,wastage_type_id,reason)
	       	values (v_company_id,0,0,param_location_id,param_inventory_stock_id,now(),v_wastage_qty,param_unit_id,4,v_reason);

	       select last_insert_id() into param_wastage_id;	

		else

			-- 
			select other_info into param_wastage_id from pos_location_inventory_stock_log as log
			where location_id=param_location_id and 
			inventory_stock_id = param_inventory_stock_id and 
			inventory_operation_id = param_inventory_operation_id and 
			operation_type_id in (6,8)
			order by created_at desc
			limit 1;


			if (param_wastage_id is not null) then 

				delete from pos_location_inventory_stock_wastage where wastage_id = param_wastage_id;

				set param_wastage_id = null;

			end if;



		end if;

	end if;




end //

delimiter ;

-- ####################################################### Wastage Percentage #####################################
