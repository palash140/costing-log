drop procedure if exists logLocationInventoryStock;
delimiter //
#create procedure logLocationInventoryStock(location_id int,inventory_stock_id int,quantity float,operation_type_id int,inventory_operation_id int,param_other_info text,param_operation_date datetime,param_parent_id int,OUT param_log_id int)
create procedure logLocationInventoryStock(param_date datetime,param_location_id int,param_inventory_stock_id int,param_operation_type_id int,param_operation_id int,param_quantity float,param_other_info text,OUT param_log_id int)
begin

	declare v_current_quantity float;
	declare v_opening_quantity float;
	declare v_last_current_quantity float; 

	declare v_operation varchar(100);
	declare v_new_current_quantity float;
	declare v_count_temp int;


	
	select current_quantity,opening_quantity 
	into v_current_quantity,v_opening_quantity
	from pos_location_inventory_stock 
	where location_id = param_location_id and inventory_stock_id = param_inventory_stock_id  
	limit 1;


	if v_current_quantity is null  then

		if v_opening_quantity is null then

			set v_last_current_quantity = 0;

		else 

			set v_last_current_quantity = v_opening_quantity; 

		end if;

	else 

		set v_last_current_quantity = v_current_quantity;

	end if;


	-- fetch math operation over quantity based on the operation_type_id
	select pos_log_operation_types.operation  into v_operation
	from pos_log_operation_types 
	where pos_log_operation_types.log_operation_type_id = param_operation_type_id;


	if v_operation = 'plus' then

		set v_new_current_quantity = v_last_current_quantity + param_quantity;

	end if;

	if v_operation = 'minus' then 

		set v_new_current_quantity = v_last_current_quantity - abs(param_quantity); 

	end if ;


	insert into 
	pos_location_inventory_stock_log(`location_id`,`inventory_stock_id`,`quantity`,`current_quantity`,`operation_type_id`,`inventory_operation_id`,`other_info`,`operation_date`)
	values(param_location_id,param_inventory_stock_id,param_quantity,v_new_current_quantity,param_operation_type_id,param_operation_id,param_other_info,param_date);

	set param_log_id = last_insert_id();

end //


delimiter ;

drop procedure if exists logManufactureInventoryRecipes;
delimiter //
create procedure logManufactureInventoryRecipes(param_recipe_type int ,param_date datetime,param_location_id int,param_inventory_stock_id int,param_manufacture_type int,param_manufacture_id int,param_quantity float,param_log_id int,param_costing_tree_node_parent_id int)
begin

	declare v_raw_product_id int;
	declare v_recipe_quantity float;
	declare v_unit_id int;
	declare v_recipe_type int;

	declare v_other_info text default null;


	declare v_manufacture_costing_tree_node_id int;



	declare log_quantity float; 
	declare recipe_consumed_quantity float;

	declare v_wastage_log_id int default null;
	declare v_revert_operation_type_id int;

	declare v_log_id int;
	declare v_lis_wastage_percentage_id int default null;
	declare v_costing_tree_node_type_id int default 2;
	declare v_costing_tree_node_id int;
	declare v_costing float default null;
	declare v_in_queue int default 0;
	declare v_temp int;

	declare v_last_log_id int;
	declare v_orignal_recipe_quantity float;





	declare v_recipes_finished int default 0;
	-- fetch current item recipie item and the quantity participation
	declare v_recipes cursor for 
	select raw_product_id,(qty * param_quantity),unit_id
	from pos_recipe_new 
	where final_product_id = param_inventory_stock_id;
	declare continue handler for not found set v_recipes_finished = 1; 



	SET @@GLOBAL.max_sp_recursion_depth = 255;
	SET @@session.max_sp_recursion_depth = 255; 

	set v_manufacture_costing_tree_node_id = param_costing_tree_node_parent_id ;

	-- initalize recipes 
	open v_recipes;

		-- loop over recipe
		recipes_loop: LOOP

			-- fetch recipe item(raw_product_id) id and quantity
			fetch v_recipes into v_raw_product_id,v_recipe_quantity,v_unit_id;

			-- is it last recipe item
			if v_recipes_finished = 1 then

				leave recipes_loop;

			end if;

			-- 13 revert / delete because of normal manufacture
			-- 11 revert / delete becaues of sale manufacture
			if param_recipe_type in (13,11) then


				set v_temp = v_temp;

			    -- 11 sale manufacture consumption revert
				-- 8 sale manufacture consumption
			 --    if param_recipe_operation = 11 then
				-- 	set v_revert_operation_type_id = 8;
				-- end if;

				-- -- 13 manufacture consumption revert
				-- -- 6 manufacture conusmption
				-- if param_recipe_operation = 13 then
				-- 	set v_revert_operation_type_id = 6;
				-- end if;
				

				-- -- its entry in log
				-- select quantity,other_info into log_quantity,v_lis_wastage_percentage_id
				-- from pos_location_inventory_stock_log
				-- where pos_location_inventory_stock_log.location_id = param_location_id and
				-- pos_location_inventory_stock_log.inventory_stock_id = v_raw_product_id and
			 --    pos_location_inventory_stock_log.operation_type_id=v_revert_operation_type_id and 
			 --    pos_location_inventory_stock_log.inventory_operation_id = param_inventory_operation_id
			 --    order by created_at desc
			 --    limit 1;


			 --    set recipe_consumed_quantity = log_quantity;


				-- -- revert the wastage percentag ie exists 
				-- if v_lis_wastage_percentage_id is not null then

				-- 	delete from pos_location_inventory_stock_wastage where wastage_id = v_lis_wastage_percentage_id;

				-- end if;



				--  call logLocationInventoryStock(param_location_id,v_raw_product_id,recipe_consumed_quantity,param_recipe_operation,param_inventory_operation_id,null,param_operation_date);

			 else 


				call logLocationInventoryStock(param_date,param_location_id,v_raw_product_id,param_recipe_type,param_manufacture_id,v_recipe_quantity,v_other_info,v_log_id);


				if isInventoryHasWastagePercentage(v_raw_product_id) then 

					call logWastagePercentage(param_date,param_location_id,v_raw_product_id,param_recipe_type,param_manufacture_id,v_recipe_quantity,v_unit_id,v_wastage_log_id,param_costing_tree_node_parent_id);

					 if v_wastage_log_id is not null then

					 	update pos_location_inventory_stock_log 
					    set other_info = v_wastage_log_id
					    where log_id=  v_log_id;


					    # to cover the current_quantity , logged by wastage
					    update pos_location_inventory_stock_log
					    set other_info = null
					    where log_id = v_wastage_log_id;

					 end if;

					set param_location_id = param_location_id;

				else

					select  createInventoryCostingTreeNode(v_log_id,v_recipe_quantity,v_costing_tree_node_type_id,param_costing_tree_node_parent_id) into v_costing_tree_node_id;

				end if;


			end if;


		end loop recipes_loop;

	close v_recipes;	

	 call calculateInventoryCostingTreeNodeCosting(param_location_id,param_inventory_stock_id,4,v_manufacture_costing_tree_node_id);

end //

delimiter ;

-- ########################################## purchase ###################################################
drop procedure if exists logDeliveredPurchaseOrder;
delimiter //
create procedure logDeliveredPurchaseOrder(param_location_id int,param_purchase_order_id int)
begin
	
	declare v_purchase_order_transaction_id int;
	declare v_inventory_stock_id int;
	declare v_purchase_qty float;
	declare v_purchase_rate float;
	declare v_purchase_order_transactions_finished int default 0;
	declare v_purchase_order_date datetime;

	declare v_parent_id int default null;
	declare v_log_id int;

	declare v_purchase_inventory_operation int default 2;

	declare v_purchase_type int default 1;


	declare v_inventory_costing_queue_id int;


	declare v_temp int;

	-- find all the purchase order transaction, id, item, quty 
	declare v_purchase_order_transactions cursor for	
	select inventory_stock_id,purchase_order_transaction_id,purchase_qty,purchase_rate
	from pos_purchase_order_transactions_new 
	where pos_purchase_order_transactions_new.purchase_order_id = param_purchase_order_id;
	declare continue handler for not found set v_purchase_order_transactions_finished  = 1;


	select purchase_order_date into v_purchase_order_date 
	from pos_purchase_orders_new 
	where pos_purchase_orders_new.purchase_order_id = param_purchase_order_id;


	open v_purchase_order_transactions;

		purchase_order_transactions_loop: loop

			fetch v_purchase_order_transactions 	
			into v_inventory_stock_id,v_purchase_order_transaction_id,v_purchase_qty,v_purchase_rate;

			if v_purchase_order_transactions_finished = 1 then

				leave purchase_order_transactions_loop;

			end if;

			#call logLocationInventoryStock(param_location_id,v_inventory_stock_id,v_purchase_qty,2,v_purchase_order_transaction_id,null,v_purchase_order_date,v_parent_id,v_log_id);

			call logLocationInventoryStock(v_purchase_order_date,param_location_id,v_inventory_stock_id,v_purchase_inventory_operation,v_purchase_order_transaction_id,v_purchase_qty,null,v_log_id);

			#call createInventoryCostingQueue(v_purchase_order_date,param_location_id,v_inventory_stock_id,1,v_purchase_order_transaction_id,v_purchase_qty,v_purchase_rate);

			if v_purchase_rate > 0 then 

				select  createInventoryCostingTransaction(v_purchase_order_date,param_location_id,v_inventory_stock_id,1,v_purchase_order_transaction_id,v_purchase_qty,v_purchase_rate) into v_temp;

			end if;
			

			# update its availabile quantity 
			-- if v_purchase_order_date < current_date() then 

			-- 	call readjustLogByDate(param_location_id,v_inventory_stock_id,v_purchase_order_date);

			-- end if;

			# insert into cotsting tree queue 
-- 			insert into pos_inventory_costing_queue values(null,v_purchase_order_date,param_location_id,v_inventory_stock_id,v_purchase_type,v_purchase_order_transaction_id,v_purchase_qty,v_purchase_rate);


-- 			set v_inventory_costing_queue_id = last_insert_id();

-- #			if isPurchaseInventoryInCostingTreeQueue(param_location_id,v_inventory_stock_id) then 
-- 			if isCostingTreeNodeInQueue(param_location_id,v_inventory_stock_id) then 

-- #			 	call consumePurchaseInventoryFromQueue(param_location_id,v_inventory_stock_id,v_purchase_order_transaction_id,v_purchase_qty,v_purchase_rate);	
-- 			 	call consumeCostingTreeNodeFromInventoryCostingQueue(param_location_id,v_inventory_stock_id,v_inventory_costing_queue_id);	
			 	

-- 			 end if ;


		end loop purchase_order_transactions_loop;

	close v_purchase_order_transactions;


end //


delimiter ;


drop procedure if exists updateParentCost;
delimiter //
create procedure updateParentCost(param_child_id int)
begin
	
	declare v_parent_id int default null;
	declare v_parent_costing float;
	
	select parent_id into v_parent_id from pos_log_items_costing_tree where id = param_child_id;

	# parent must exists 
	if (v_parent_id is not null) then 

		# none child must be in queue
		if not exists (select * from pos_log_items_costing_tree where parent_id = v_parent_id and costing is null) then

			select sum(costing) into v_parent_costing 
			from pos_log_items_costing_tree  where parent_id = v_parent_id	;

			update pos_log_items_costing_tree 
			set costing = v_parent_costing
			where id  = v_parent_id;


		else

			update pos_log_items_costing_tree 
			set costing = null 
			where id  = v_parent_id;

		end if;

		call updateParentCost(v_parent_id); 
		
	end if ;


end //







drop procedure if exists createCostingTreeNode;
delimiter //
#create procedure createCostingTreeNode(param_log_id int,param_parent_id int,param_reference_id int,param_quantity float,param_costing float,param_node_type int,in_queue int,OUT param_node_id int)
create procedure createCostingTreeNode(param_node_type_id int,param_operation_id int, param_quantity float,param_costing float,param_log_id int,param_parent_id int,in_queue int,OUT param_node_id int)
begin

insert into pos_log_items_costing_tree
  values(null,param_log_id,param_parent_id,param_operation_id,param_quantity,param_costing,param_node_type_id,in_queue);

set param_node_id = last_insert_id();

end //



delimiter ;
drop procedure if exists createAvailablePurchaseInventoryQueue;
delimiter //
create procedure createAvailablePurchaseInventoryQueue(param_location_id int, param_inventory_stock_id int)
begin
	# fist get the current_quantity
	declare v_current_quantity float;
	declare v_purchase_order_transaction_ids varchar(100) default '0';
	declare v_delivered_purcahse_order int default 4;

	declare v_purchase_order_date datetime;
	declare v_purchase_quantity float;
	declare v_purchase_rate float;
	declare v_purchase_order_transaction_id int;


	DROP TEMPORARY TABLE IF EXISTS pos_available_purchase_item_queue;

	
	CREATE TEMPORARY TABLE `pos_available_purchase_item_queue` (
	  `purchase_order_transaction_id` int(11) NOT NULL,
	  `purchase_quantity` decimal(10,2) NOT NULL,
	  `purchase_rate` float(10,2) NOT NULL,
	  `purchase_date` datetime NOT NULL
	) ;



	select current_quantity  into v_current_quantity
	from pos_location_inventory_stock
	where location_id = param_location_id and inventory_stock_id = param_inventory_stock_id limit 1;

	delete from pos_available_purchase_item_queue;

	if v_current_quantity > 0  then

		while v_current_quantity > 0 DO

			select purchase_order_date,purchase_qty,purchase_rate,purchase_order_transaction_id
			into v_purchase_order_date,v_purchase_quantity,v_purchase_rate,v_purchase_order_transaction_id
			from pos_purchase_order_transactions_new as trans inner join pos_purchase_orders_new as purorder
			on purorder.purchase_order_id = trans.purchase_order_id
			where location_id = param_location_id and inventory_stock_id = param_inventory_stock_id 
			and purchase_order_status_id = v_delivered_purcahse_order
			and purchase_order_transaction_id not in (select purchase_order_transaction_id from pos_available_purchase_item_queue)
			order by purchase_order_date desc
			limit 1;

			if FOUND_ROWS() > 0 then 

				# purchase item founds
				if v_purchase_quantity > v_current_quantity then

					set v_purchase_quantity = v_current_quantity;

				end if;

				set v_current_quantity = v_current_quantity - v_purchase_quantity;


				insert into pos_available_purchase_item_queue	
				values(v_purchase_order_transaction_id,v_purchase_quantity,v_purchase_rate,v_purchase_order_date);


			else

				# to get out of loop
				set v_current_quantity = 0; 

			end if;

		end while ;

	end if;


end //


delimiter ;

drop function if exists isPurchaseInventoryInQueue;
delimiter //
create function isInventoryInCostingQueue(param_location_id int, param_inventory_stock_id int)
returns boolean
begin

	declare v_type_id int;
	declare v_purchase_inventory int default 1;
	declare v_manufacture_inventory int default 2;

	if isPurchaseInventory(param_location_id,param_inventory_stock_id) then

		set v_type_id = v_purchase_inventory;

	else

		set v_type_id = v_manufacture_inventory ;

	end if;


	if exists(select * from pos_inventory_costing_queue where location_id = param_location_id and inventory_stock_id = param_inventory_stock_id and type_id = v_type_id ) then

		return true;

	end if;

	return false; 

end //

delimiter ;



drop function if exists isCostingTreeNodeInQueue;
delimiter //
create function isCostingTreeNodeInQueue(param_location_id int, param_inventory_stock_id int)
returns boolean
begin

		declare v_node_type int default 0;
		declare v_purchase_inventory int default 1;
		declare v_manufacture_inventory int default 6;

		if isPurchaseInventory(param_location_id,param_inventory_stock_id) then

			set v_node_type = v_purchase_inventory;

		else	

			set v_node_type = v_manufacture_inventory;

		end if;


		if exists( select * from pos_location_inventory_stock_log as poslog inner join pos_log_items_costing_tree as tree
				on poslog.log_id = tree.log_id
				where location_id = param_location_id and inventory_stock_id=param_inventory_stock_id and node_type_id = v_node_type
				and  in_queue = 1
			) then

		return true;

		end if;


		return false; 

end //


delimiter ;



drop procedure if exists logWastagePercentage;
delimiter //
create procedure logWastagePercentage(param_date datetime,param_location_id int,param_inventory_stock_id int,param_manufacture_consumption_type_id int,param_manufacture_id int,param_consumed_quantity float,param_unit_id int,out param_wastage_id int,param_costing_tree_node_id int)

begin

	declare v_wastage_percentage float default 0 ;
	declare v_last_log_operation varchar(100); 
	declare v_company_id int;

	declare v_total_consumed_quantity float;
	declare v_wastage_qty float;
	declare v_log_id int;
	declare v_wastage_type int default 5;
	declare v_other_info text default null;

	declare v_reason text;
	declare v_parent_id int;
	declare v_temp int;

  # wastage of manufacture
	declare v_costing_tree_node_type_id int default 2; 
	declare v_in_queue int default 0;
	declare v_costing_tree_node_id int;

	select cast(wastage_percentage as decimal(10,2)) into v_wastage_percentage
	from pos_inventory_stock_new 
	where pos_inventory_stock_new.inventory_stock_id = param_inventory_stock_id;

	if v_wastage_percentage  > 0  then


		select ((100 * param_consumed_quantity)/(100- v_wastage_percentage)) into v_total_consumed_quantity;
		set v_wastage_qty= v_total_consumed_quantity - param_consumed_quantity; 


		-- 6 normal manufacture consumption,
		-- 8 sale manufacture consumpktion
		if param_manufacture_consumption_type_id in (6,8)  then

			select company_id into v_company_id from pos_location where pos_location.location_id = param_location_id;

			-- 4 wastage type, means beacuase of manufcature comsumpttion
			select concat("{'manufcature_id':",param_manufacture_id,"}") into v_reason;

			-- insert the entry as wastage 
			-- 4 waste because of manufacture consumption
			insert into 
				pos_location_inventory_stock_wastage (company_id,parent_id,employee_id,location_id,inventory_stock_id,wastage_date,wastage_quantity,unit_id,wastage_type_id,reason)
				values (v_company_id,0,0,param_location_id,param_inventory_stock_id,now(),v_wastage_qty,param_unit_id,4,v_reason);

			select last_insert_id() into param_wastage_id;	

			call logLocationInventoryStock(param_date,param_location_id,param_inventory_stock_id,v_wastage_type,param_wastage_id,v_wastage_qty,v_other_info,v_log_id);

			select  createInventoryCostingTreeNode(v_log_id,v_total_consumed_quantity,v_costing_tree_node_type_id,param_costing_tree_node_id) into v_temp;

			set param_wastage_id = v_log_id ;

		else

			-- 
			select other_info into param_wastage_id from pos_location_inventory_stock_log as log
			where location_id=param_location_id and 
			inventory_stock_id = param_inventory_stock_id and 
			inventory_operation_id = param_inventory_operation_id and 
			operation_type_id in (6,8)
			order by created_at desc
			limit 1;


			if (param_wastage_id is not null) then 

				delete from pos_location_inventory_stock_wastage where wastage_id = param_wastage_id;

				set param_wastage_id = null;

			end if;



		end if;

	end if;




end //

delimiter ;



drop procedure if exists createInventoryInCostingQueue;
delimiter //
create procedure createInventoryCostingInQueue(param_operation_date datetime,param_location_id int, param_inventory_stock_id int,param_operation_type_id int,param_operation_id int,param_quantity float,param_rate float,OUT param_costing_queue_id int)
begin

	insert into pos_inventory_costing_queue values(null,param_operation_date,param_location_id,param_inventory_stock_id,param_operation_type_id,param_operation_id,param_quantity,param_rate);

	set param_costing_queue_id = last_insert_id();



end //
delimiter ;


drop procedure if exists createCurrentQuantityToCostingQueue;
delimiter //
create procedure createCurrentQuantityToCostingQueue(param_location_id int, param_inventory_stock_id int)
begin

-- use cases 
-- case 1 current_quantity is null done
-- case 2 current_quantity is 0 done 
-- case 3 current_quantity exists but no transactions  done 
-- case 4 current_quantity exists and transaction also exists and along with order date
-- 	case 41 current_quantity exists with transaction exists in less quanity 
-- 	case 42 current_quantity exists with transaction exists in equal quantity done 
-- 	case 43 current_quantity exists with transaction exists in more  quantity  with cover direct or total trnsaction done 
-- 	case 44 current_quantity exists with transaction exists in more  quantity with cover with partial transaction done 

-- conclustion: same date, last one pick first from transcation 

-- doubt: how to handle,if current quantity greater than the available purchaes quantity 






	declare v_current_quantity float;
	declare v_operation_date datetime;
	declare v_operation_type_id int;
	declare v_operation_id int;
	declare v_quantity float;
	declare v_rate float;

	declare v_delivered_purcahse_order int default 4;
	declare v_purchase_inventory int default 1;
	declare v_manufacture_inventory int default 2;



	select current_quantity into v_current_quantity from pos_location_inventory_stock
	where location_id = param_location_id and inventory_stock_id = param_inventory_stock_id;


	
			

	# null on first creation 
	if v_current_quantity > 0 then

		if isPurchaseInventory(param_location_id,param_inventory_stock_id) then

			set v_operation_type_id = v_purchase_inventory;	

		else 

			set v_operation_type_id = v_manufacture_inventory;	

		end if;


		while v_current_quantity != 0 do 


			if v_operation_type_id = v_purchase_inventory then 	

				# last operation over quantity 	
				select purchase_order_date,purchase_order_transaction_id,purchase_qty,purchase_rate
				into v_operation_date,v_operation_id,v_quantity,v_rate
				from pos_purchase_orders_new as purorder inner join pos_purchase_order_transactions_new	 as purtrans
				on purorder.purchase_order_id = purtrans.purchase_order_id 
				where purchase_order_status_id = v_delivered_purcahse_order and 
				location_id = param_location_id and 
				inventory_stock_id = param_inventory_stock_id and 
				purchase_order_transaction_id not in ( select operation_id from pos_inventory_costing_queue where location_id = param_location_id and inventory_stock_id = param_inventory_stock_id)
				order by purchase_order_date asc
				limit 1;

			else

				# manufacture inventory	
				set v_current_quantity = v_current_quantity;


			end if ;


			if FOUND_ROWS()	>  0  then


				# first queue transaction quantity is more than requried, means partially accept the trandsdaction 
				if v_quantity > v_current_quantity  then

					set v_quantity = v_current_quantity ;

				end if;	


				set v_current_quantity = v_current_quantity - v_quantity ;

				call createInventoryCostingQueue(v_operation_date,param_location_id,param_inventory_stock_id,v_operation_type_id,v_operation_id,v_quantity,v_rate);

			else 

				# possible not all current quantity transaction available
				set v_current_quantity = 0;

			end if;




		end while ;

	end if ;

end //
delimiter ;


drop function if exists isInventoryInCostingQueue;
delimiter //
create function isInventoryInCostingQueue(param_location_id int,param_inventory_stock_id int)
returns boolean
begin

	if exists (select * from pos_inventory_costing_queue where location_id = param_location_id and inventory_stock_id = param_inventory_stock_id limit 1) then

		return true;

	end if;	

	return false;

end //
delimiter ;


drop procedure if exists createInventoryCostingNodeFromCostingQueue;
delimiter //
create procedure createInventoryCostingNodeFromCostingQueue(param_location_id int, param_inventory_stock_id int,param_node_id int ,INOUT param_quantity float,param_parent_id int,param_log_id int)
begin 

	# test cases
	# 1) if not in queue, must be queued
	# 2) In queue and available completly
	# 3) In queue but available partially, 
	
	declare v_operation_id int default null;
	declare v_quantity float;
	declare v_rate float;
	declare v_in_queue int default 0;
	declare v_costing float default null;
	declare v_costing_queue_id int;
	declare v_costing_node_id int;


	if isInventoryInCostingQueue(param_location_id,param_inventory_stock_id) then

		select id,operation_id,quantity,rate  
		into v_costing_queue_id,v_operation_id,v_quantity,v_rate
		from pos_inventory_costing_queue
		where location_id = param_location_id and inventory_stock_id = param_inventory_stock_id
		order by id asc
		limit 1;


		if v_quantity > param_quantity then

			set v_quantity = param_quantity;

			update pos_inventory_costing_queue
			set quantity = quantity - param_quantity
			where id = v_costing_queue_id;

		else

			delete from pos_inventory_costing_queue
			where id= v_costing_queue_id;

		end if;


		set v_costing = v_quantity * v_rate;

		insert into pos_log_items_costing_tree
		values (null,param_log_id,param_parent_id,v_operation_id,v_quantity,v_costing,param_node_id,v_in_queue);


		set param_quantity = param_quantity - v_quantity;	

		# required quantity consumed completly
		if param_quantity = 0 then

		    set v_costing_queue_id = last_insert_id();

			# finalize the costing to parent
			call updateParentCost(v_costing_queue_id);

		else

			# find next costing transaction 
		    call createInventoryCostingNodeFromCostingQueue(param_location_id,param_inventory_stock_id,param_node_id,param_quantity,param_parent_id,param_log_id);

	    end if;

	 else

	    set v_in_queue = 1;

	 	insert into pos_log_items_costing_tree
		values (null,param_log_id,param_parent_id,v_operation_id,param_quantity,v_costing,param_node_id,v_in_queue);

	end if;


end //
delimiter ;


drop function if exists isInventoryInCostingTreeQueue;
delimiter //
create function isInventoryInCostingTreeQueue(param_location_id int, param_inventory_stock_id int)
returns boolean 
begin

	declare v_total_rows int;

	select count(*) into  v_total_rows
	from pos_location_inventory_stock_log as lislog inner join pos_log_items_costing_tree as costtree  
	on lislog.log_id = costtree.log_id
	where location_id = param_location_id and inventory_stock_id = param_inventory_stock_id 
	and in_queue = 1 
	limit 1;

	if v_total_rows = 0 then

		return false;

	else

		return true;

	end if;

end //
delimiter ;

drop procedure if exists consumeInventoryFromCostingQueue;
delimiter //
create procedure consumeInventoryFromCostingQueue(param_location_id int, param_inventory_stock_id int,param_inventory_costing_queue_id int,IN param_quantity float,param_rate float)
begin

	declare v_costing_node_id int;
	declare v_costing_node_quantity float;
	declare v_costing_node_type_id int;
	declare v_costing_node_parent_id int;
	declare v_costing_node_log_id int;
	declare v_costing_node_operation_id int default null;
	declare v_in_queue int default 1;
	declare v_costing float default null;

	declare v_finished int default 0;
	declare v_costing_tree_queued_inventory cursor for 
	select id,node_type_id,costtree.quantity,parent_id,costtree.log_id
	from pos_log_items_costing_tree as costtree inner join pos_location_inventory_stock_log as lislog
	on lislog.log_id = costtree.log_id
	where location_id = param_location_id and inventory_stock_id = param_inventory_stock_id  and
	in_queue = 1
	order by operation_date asc;
	declare handler for not found set v_finished = 1;


	open v_costing_tree_queued_inventory ;

		costing_queued_inventory: loop	

			fetch v_costing_tree_queued_inventory into v_costing_node_id,v_costing_node_type_id,v_costing_node_quantity,v_costing_node_parent_id,v_costing_node_log_id;

			if v_finished = 1 || param_quantity = 0 then

				# inventory coting queue finished 
				if param_quantity = 0 then

					delete from pos_inventory_costing_queue where id = param_inventory_costing_queue_id;

				else
					# inventory_coting_queue quantity did  not finished complitly

					 update pos_inventory_costing_queue 
					 set quantity = param_quantity
					 where id =  param_inventory_costing_queue_id;

				end if;

				leave costing_queued_inventory;

			end if;

			 #queued one is greater than tha paramter 
			if param_consumed_quantity < v_costing_node_quantity then

				set v_costing_node_quantity = param_consumed_quantity;

				# add remaing into queue with other quantity 
				call createCostingTreeNode(v_costing_node_type_id,v_costing_node_operation_id,v_costing_node_quantity,v_costing,v_costing_node_parent_id,v_costing_node_log_id,v_in_queue,v_costing_node_id);

				insert 

			end if;	

			update pos_log_items_costing_tree 
			set costing = v_costing_node_quantity * param_rate,
				quantity = v_costing_node_quantity,
				in_queue = 0 
			where id = v_costing_node_id;

			SET @@GLOBAL.max_sp_recursion_depth = 255;
			SET @@session.max_sp_recursion_depth = 255; 

			call updateParentCost(v_costing_node_id);
			
			set param_quantity = param_quantity - v_costing_node_quantity ;


		end loop costing_queued_inventory ;

	close v_costing_tree_queued_inventory;
	
end //
delimiter ;



drop procedure if exists createCostingTreeNode;
delimiter //
#create procedure createCostingTreeNode(param_log_id int,param_parent_id int,param_reference_id int,param_quantity float,param_costing float,param_node_type int,in_queue int,OUT param_node_id int)
create procedure createCostingTreeNode(param_node_type_id int,param_operation_id int, param_quantity float,param_costing float,param_log_id int,param_parent_id int,in_queue int,OUT param_node_id int)
begin

insert into pos_log_items_costing_tree
  values(null,param_log_id,param_parent_id,param_operation_id,param_quantity,param_costing,param_node_type_id,in_queue);

set param_node_id = last_insert_id();

end //
