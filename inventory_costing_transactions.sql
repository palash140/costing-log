# inventory_costing_transaction are those which will  help other inventory to get calculated costing
# these are like purchase one, manufacturing one
# will help to get calculate costing to manufacturing one , sale one etc 

# the story line is, at any time, at any location, at any inventory , any time of costing_transaction happened, just create its entry here
# we store 
# date -  the time 
# location_id - at which location 
# inventory_stock_id - to which inventory 
# type_id - (which type of costing transcation it is )
# transaction_id - the id of that transaction , use for reference 
# quantity - the quantity of costing 
# rate- rate of individual one


create table if not exists pos_inventory_costing_transactions(
	id int auto_increment primary key ,
	transaction_date datetime ,
	location_id int ,
	inventory_stock_id int ,
	type_id int ,
	transaction_id int , 
	quantity float(10,3),
	rate float 
);


drop function if exists createInventoryCostingTransaction;
delimiter //
create function createInventoryCostingTransaction(param_transcation_date datetime,param_location_id int, param_inventory_stock_id int, param_type_id int, param_transaction_id int,param_quantity float, param_rate float)
returns boolean
begin

	insert into pos_inventory_costing_transactions values(null,param_transcation_date,param_location_id,param_inventory_stock_id,param_type_id,param_transaction_id,param_quantity,param_rate);

	return last_insert_id();

end //
delimiter ;



drop procedure if exists  updateInventoryCostingTranscationQuantity;
delimiter //
create procedure updateInventoryCostingTranscationQuantity(param_inventory_costing_transaction_id int, param_quantity float)
begin

	update pos_inventory_costing_transactions
	set quantity = param_quantity
	where id = param_inventory_costing_transaction_id;

end //
delimiter ;



drop procedure if exists deleteInventoryCostingTransaction;
delimiter //
create procedure deleteInventoryCostingTransaction(param_inventory_costing_transaction_id int)
begin

	delete from pos_inventory_costing_transactions where id = param_inventory_costing_transaction_id; 

end //
delimiter ;



drop procedure if exists calculateInventoryCosting;
delimiter //
create procedure calculateInventoryCosting(param_location_id int, param_inventory_stock_id int, INOUT param_quantity float,INOUT param_costing float, INOUT param_transaction_id int,INOUT param_consting_consumed_quantity float)
begin

	declare v_costing float;
	declare v_quantity float;
	declare v_rate float;
	declare v_inventory_costing_transaction_id int;

	select id,transaction_id,quantity,rate into v_inventory_costing_transaction_id,param_transaction_id,v_quantity,v_rate
	from pos_inventory_costing_transactions
	where location_id = param_location_id and inventory_stock_id = param_inventory_stock_id 
	limit 1;


	if param_quantity >= v_quantity then

		# remove the costing transaction 
		delete from pos_inventory_costing_transactions where id = v_inventory_costing_transaction_id ;

	else 

		#update the costing transaction 
		update pos_inventory_costing_transactions
		set quantity = quantity - param_quantity
		where id = v_inventory_costing_transaction_id;

		set v_quantity = param_quantity;

	end if;

	set param_costing=v_rate * param_quantity ;

	set param_consting_consumed_quantity = v_quantity;

	set param_quantity  = param_quantity - v_quantity;


end //
delimiter ;



drop function if exists hasInventoryCostingTransaction ;
delimiter //
create function hasInventoryCostingTransaction(param_location_id int, param_inventory_stock_id int)
returns boolean 
begin

	if exists (select * from pos_inventory_costing_transactions where location_id = param_location_id and inventory_stock_id = param_inventory_stock_id limit 1) then

		return true;

	end if;

	return false;

end //





delimiter ;

drop procedure if exists createInventoryCostingTransactionsFromCurrentQuantity;
delimiter //
create procedure createInventoryCostingTransactionsFromCurrentQuantity(param_location_id int,param_inventory_stock_id int)
begin 

	declare v_current_quantity float;
	declare v_delivered_purhcase_order int default 4;

	declare v_transaction_date datetime;
	declare v_transaction_id int;
	declare v_quantity float;
	declare v_rate float;


	select current_quantity into v_current_quantity 
	from pos_location_inventory_stock 
	where location_id = param_location_id and inventory_stock_id = param_inventory_stock_id 
	limit 1;

	while v_current_quantity > 0 DO

		select purchase_order_date,purchase_order_transaction_id,purchase_qty,purchase_rate
		into v_transaction_date,v_transaction_id,v_quantity,v_rate
		from pos_purchase_orders_new as purorder inner join pos_purchase_order_transactions_new as purtrans
		on purorder.purchase_order_id = purtrans.purchase_order_id
		where location_id = param_location_id and inventory_stock_id = param_inventory_stock_id
		and purchase_order_status_id = v_delivered_purhcase_order		
		and purchase_rate > 0 
		and purchase_order_transaction_id not in (select transaction_id from pos_inventory_costing_transactions where location_id = param_location_id and inventory_stock_id = param_inventory_stock_id)
		order by purchase_order_date desc
		limit 1;

		if  found_rows()  != 0 then

			if v_current_quantity < v_quantity then

				set v_quantity = v_current_quantity;

			end if;

			select createInventoryCostingTransaction(v_transaction_date,param_location_id,param_inventory_stock_id,1,v_transaction_id,v_quantity,v_rate);

			set v_current_quantity = v_current_quantity - v_quantity;

		else

			set v_current_quantity = 0;

		end if;


	end while;

end //

delimiter ;

#call createInventoryCostingTransactionsFromCurrentQuantity(15,3726);


drop procedure if exists temp;
delimiter //
create procedure temp()
begin 

	truncate pos_inventory_costing_transactions;
	truncate pos_inventory_costing_tree;

	update pos_location_inventory_stock set current_quantity = 4 where inventory_stock_id = 3726;
	update pos_location_inventory_stock set current_quantity = 1 where inventory_stock_id = 3727;
	update pos_location_inventory_stock set current_quantity = 1 where inventory_stock_id = 3728;

	call createInventoryCostingTransactionsFromCurrentQuantity(15,3726);
	call createInventoryCostingTransactionsFromCurrentQuantity(15,3727);
	call createInventoryCostingTransactionsFromCurrentQuantity(15,3728);


end //

delimiter ;



drop procedure if exists temp;
delimiter //
create procedure temp1()
begin 


	call createInventoryCostingTransactionsFromCurrentQuantity(15,328);
	call createInventoryCostingTransactionsFromCurrentQuantity(15,330);
	call createInventoryCostingTransactionsFromCurrentQuantity(15,348);
	call createInventoryCostingTransactionsFromCurrentQuantity(15,349);
	call createInventoryCostingTransactionsFromCurrentQuantity(15,351);
	call createInventoryCostingTransactionsFromCurrentQuantity(15,352);
	call createInventoryCostingTransactionsFromCurrentQuantity(15,364);
	call createInventoryCostingTransactionsFromCurrentQuantity(15,368);
	call createInventoryCostingTransactionsFromCurrentQuantity(15,394);


end //

delimiter ;



