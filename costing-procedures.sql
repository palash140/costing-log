drop procedure if exists enqueuePurchaseInventoryLogwithQuantity;
delimiter //
create procedure enqueuePurchaseInventoryLogwithQuantity(param_log_id int,param_required_quantity float)
begin 

		insert into 
			pos_purchase_inventory_consumer_queue(id,log_id,consuming_quantity) 
			values(NULL,param_log_id,abs(param_required_quantity));

end //
delimiter ;

drop function if exists isPurchaseInventory;
delimiter //
create function isPurchaseInventory(param_location_id int, param_inventory_stock_id int)
returns boolean
begin
	
	declare v_has_manufacturing int;	

		# look for item is queueable or not
	select has_manufacturing into v_has_manufacturing
	from pos_location_inventory_stock
	where location_id=param_location_id and inventory_stock_id= param_inventory_stock_id;


	if v_has_manufacturing = 0 then 

		return true;

	end if;

	return false;


end //
delimiter ;




drop procedure if exists consumeCostingTreeNodeFromInventoryCostingQueue;
delimiter //
create procedure consumeCostingTreeNodeFromInventoryCostingQueue(param_location_id int,param_inventory_stock_id int,param_inventory_costing_queue_id int)
begin

	
	declare v_reaming_quantity_to_consume float;

	declare v_log_id int;
	declare v_node_id int;
	declare v_consuming_quantity float;
	declare v_parent_id int;
	declare v_temp int;

	declare v_costing_tree_node_type int default 1;
	declare v_costing_tree_node_id int;
	declare v_in_queue int default 0;

	declare v_transaction_id int;
	declare v_rate float;
	declare v_quantity float;

	declare v_purchase_inventory int default 1;
	declare v_manufacture_inventory int default 6;


	declare v_queue_cleared int default 0;	
	declare v_queue_items cursor for 
	select id,tree.log_id,tree.quantity,tree.parent_id
	from pos_location_inventory_stock_log  as poslog
	inner join pos_log_items_costing_tree  as tree
	on  poslog.log_id = tree.log_id
	where location_id=param_location_id and inventory_stock_id = param_inventory_stock_id
	and in_queue = 1 and node_type_id = IF(isPurchaseInventory(param_location_id,param_inventory_stock_id),v_purchase_inventory,v_manufacture_inventory)
	order by id; 	 # queue order
	declare CONTINUE handler for not found set v_queue_cleared = 1;


	if isPurchaseInventory(param_location_id,param_inventory_stock_id) then

		set v_costing_tree_node_type = v_purchase_inventory;

	else

		set v_costing_tree_node_type = v_manufacture_inventory;


	end if;

	
	select transaction_id,quantity,rate  into v_transaction_id,v_quantity,v_rate
	from pos_inventory_costing_queue 
	where id = param_inventory_costing_queue_id;

	set v_reaming_quantity_to_consume = v_quantity;

	SET @@GLOBAL.max_sp_recursion_depth = 255;

	SET @@session.max_sp_recursion_depth = 255; 

	open v_queue_items;

		queue_items_loop: loop

			fetch v_queue_items into v_node_id,v_log_id,v_consuming_quantity,v_parent_id;

			# queue cleared           # all purchase quantity consumed
			if v_queue_cleared = 1 || v_reaming_quantity_to_consume = 0 then 


				if v_reaming_quantity_to_consume = 0 then

					delete from  pos_inventory_costing_queue where id = param_inventory_costing_queue_id;

				else

					update pos_inventory_costing_queue 
					set quantity =  v_reaming_quantity_to_consume
					where id = param_inventory_costing_queue_id;

				end if;


				leave queue_items_loop;

			end if ;


			if v_reaming_quantity_to_consume >= v_consuming_quantity then

				# dequeue item 
				update pos_log_items_costing_tree 
				set operation_id=v_transaction_id,
					in_queue = 0 ,	
					costing = v_consuming_quantity * v_rate
				where id=v_node_id;

				call updateParentCost(v_node_id);

				# on last quue, it will set to remaning 0
				set v_reaming_quantity_to_consume = v_reaming_quantity_to_consume - v_consuming_quantity;


			else 

				#last queue item and  qty is large than remaing quty 

				#update the remaing quantity in queue to be comsumed 
				update pos_log_items_costing_tree
				set quantity = (v_consuming_quantity - v_reaming_quantity_to_consume)
				where id = v_node_id;

				#call createCostingTreeNode(null,v_parent_id,param_purchase_order_transaction_id,v_reaming_quantity_to_consume,v_reaming_quantity_to_consume * v_rate,v_costing_tree_node_type,v_in_queue,v_costing_tree_node_id);
				call createCostingTreeNode(v_costing_tree_node_type,v_transaction_id,v_reaming_quantity_to_consume,v_reaming_quantity_to_consume * v_rate,v_log_id,v_parent_id,v_in_queue,v_costing_tree_node_id);

				call updateParentCost(v_costing_tree_node_id);

				set v_reaming_quantity_to_consume = 0;


			end if; 


		

			## trigger to calculate costing of node		
		end loop queue_items_loop;

	close v_queue_items;
		
end //
delimiter ;





drop procedure if exists addLogItemCostingTransaction ;

delimiter //
create procedure addLogItemCostingTransaction(param_log_id int, param_purchase_transaction_id int, param_transaction_qty float, param_transaction_cost float)
begin
	
	insert into pos_log_item_costing_transactions 
		values(null,param_log_id,param_purchase_transaction_id,param_transaction_qty,param_transaction_cost);


end //

delimiter ;

#log item means any used puchase inventory, has to calculate costing transactions
drop procedure if exists createCostingTreeNodeFromInventoryCostingQueue;
delimiter //
create procedure createCostingTreeNodeFromInventoryCostingQueue(param_location_id int,param_inventory_stock_id int,INOUT param_qty_transactions float,param_log_id int,param_parent_id int)
begin

	# qty_transations, the qty, whose trnsactions to be calculated 

	# the  qty, that transation will be snapshot
	declare v_transaction_qty float;		
	declare v_quantity float default 0;
	declare v_transaction_id int;
	declare v_rate float;
	declare v_delivered_purcahse_order int default 4;
	declare v_in_queue int default 0;
	declare v_queue_id int;


	declare v_type_id int;
	declare v_purchase_inventory int default 1;
	declare v_manufacture_inventory int default 2;


	declare v_costing_tree_node_type int default 1; 
	declare v_costing_tree_node_id int;
	declare v_purchase_inventory_transaction int default 1;
	declare v_manufacture_inventory_transaction int default 2;



	# purchaes order transaction


	declare v_queue_cleared int default 0;
	declare v_queue_items cursor for 
	select id,transaction_id,quantity,rate 
	from pos_inventory_costing_queue
	where location_id = param_location_id and inventory_stock_id = param_inventory_stock_id 
	and type_id = v_type_id
	order by operation_date  asc; 
	declare CONTINUE handler for not found set v_queue_cleared = 1;




	if isPurchaseInventory(param_location_id,param_inventory_stock_id) then

		set v_type_id = v_purchase_inventory;
		set v_costing_tree_node_type = v_purchase_inventory_transaction;

	else

		set v_type_id = v_manufacture_inventory ;
		set v_costing_tree_node_type = v_manufacture_inventory_transaction;

	end if;





	SET @@GLOBAL.max_sp_recursion_depth = 255;
	SET @@session.max_sp_recursion_depth = 255; 

	open v_queue_items ;

		queue_items_loop: loop	

			fetch v_queue_items into v_queue_id,v_transaction_id,v_quantity,v_rate;

			if v_queue_cleared = 1 || param_qty_transactions = 0 then

				leave queue_items_loop;

			end if;	



			if param_qty_transactions >=  v_quantity then

				set v_transaction_qty = v_quantity;

				# remove purchase transaction from queue
				delete from pos_inventory_costing_queue where  id = v_queue_id;

			else

				# update the quantity 
				update pos_inventory_costing_queue
				set quantity = quantity - param_qty_transactions  
				where id = v_queue_id;

				set v_transaction_qty = param_qty_transactions;

			end if;

			call createCostingTreeNode(v_costing_tree_node_type,v_transaction_id,v_transaction_qty,v_transaction_qty * v_rate,param_log_id,param_parent_id,v_in_queue,v_costing_tree_node_id);


			# last transaction node 
			if (param_qty_transactions <= v_quantity ) then 

				# go for update the parent
		        call updateParentCost(v_costing_tree_node_id);

			end if;

			# update the qty_transactions 
			set param_qty_transactions = param_qty_transactions - v_transaction_qty;

		end loop queue_items_loop;

	close v_queue_items;


end //
delimiter ;




drop function if exists isInventoryHasWastagePercentage;
delimiter //
create function isInventoryHasWastagePercentage(param_inventory_stock_id int)
returns boolean
begin
	if exists(select * from pos_inventory_stock_new   where inventory_stock_id= param_inventory_stock_id and wastage_percentage > 0) then

		return true;

	end if;


	return false;

end //
delimiter ;


drop procedure if exists createCostingQueue;
delimiter //
create procedure createCostingQueue(param_location_id int, param_inventory_stock_id int)
begin

	declare v_type_id int;
	declare v_purchase_inventory int default 1;
	declare v_manufacture_inventory int default 2;
	declare v_delivered_purcahse_order int default 4;

	declare v_current_quantity float;

	declare v_purchase_order_date datetime;
	declare v_purchase_order_transaction_id int;
	declare v_purchase_quantity float ;
	declare v_purchase_rate float;


	if isPurchaseInventory(param_location_id,param_inventory_stock_id) then

		set v_type_id = v_purchase_inventory;

	else

		set v_type_id = v_manufacture_inventory ;

	end if;


	select current_quantity  into v_current_quantity
	from pos_location_inventory_stock
	where location_id = param_location_id and inventory_stock_id = param_inventory_stock_id limit 1;


	if v_current_quantity > 0 then


		if v_type_id = 1 then


			while v_current_quantity > 0 DO

				select purchase_order_date,purchase_qty,purchase_rate,purchase_order_transaction_id
				into v_purchase_order_date,v_purchase_quantity,v_purchase_rate,v_purchase_order_transaction_id
				from pos_purchase_order_transactions_new as trans inner join pos_purchase_orders_new as purorder
				on purorder.purchase_order_id = trans.purchase_order_id
				where location_id = param_location_id and inventory_stock_id = param_inventory_stock_id 
				and purchase_order_status_id = v_delivered_purcahse_order
				and purchase_order_transaction_id not in (select transaction_id from pos_inventory_costing_queue where location_id = param_location_id and inventory_stock_id = param_inventory_stock_id and type_id = v_type_id)
				order by purchase_order_date desc
				limit 1;


				if FOUND_ROWS() > 0 then 

					# purchase item founds
					if v_purchase_quantity > v_current_quantity then

						set v_purchase_quantity = v_current_quantity;

					end if;

					set v_current_quantity = v_current_quantity - v_purchase_quantity;


					insert into pos_inventory_costing_queue
					values(null,v_purchase_order_date,param_location_id,param_inventory_stock_id,v_type_id,v_purchase_order_transaction_id,v_purchase_quantity,v_purchase_rate);

				else

					# to get out of loop
					set v_current_quantity = 0; 

				end if;

			end while ;


		end if;


	end if;



end //
delimiter ;



-- drop function if exists isRequirePurchaseInventoryAvailableQuantity;
-- delimiter //
-- create function isRequirePurchaseInventoryAvailableQuantity(param_location_id int,param_inventory_stock_id)
-- returns boolean 
-- begin 
	
-- 	declare v_current_quantity float;	

-- 	select current_quantity into v_current_quantity
-- 	from pos_location_inventory_stock where location_id = param_location_id and inventory_stock_id = param_inventory_stock_id;

-- 	if v_current_quantity < 0 then
-- 		return true;
-- 	else
-- 		return false;
-- 	end if;

-- end //
-- delimiter ;
/* 
update pos_location_inventory_stock
set current_quantity = 0
where inventory_stock_id in (3726,3727,3728,3729);

3729 is main pizza to be manufactured
update pos_location_inventory_stock set current_quantity = 4 where location_inventory_stock_id = 3908;
update pos_location_inventory_stock set current_quantity = 1 where location_inventory_stock_id = 3909;
update pos_location_inventory_stock set current_quantity = 1 where location_inventory_stock_id = 3910;
update pos_location_inventory_stock set current_quantity = 0 where location_inventory_stock_id = 3911;
delete from pos_log_items_costing_tree;
*/



