drop procedure if exists updateInventoryAvailableRatePerUnit;
delimiter //
create procedure updateInventoryAvailableRatePerUnit(param_location_id int, param_inventory_stock_id int)
begin

	declare v_current_quantity float;

	declare v_purchase_order_transaction_id int;
	declare v_purchase_qty float;
	declare v_purchase_rate float ;

																# set default current rate
	select current_quantity,cost_price into v_current_quantity,v_purchase_rate   
	from pos_location_inventory_stock 
	where location_id = param_location_id and inventory_stock_id =param_inventory_stock_id;


	# has current quantity and ourchaes transaction exists 
	if v_current_quantity > 0  then

		# fifo to get latest cost per unit 	
		drop temporary table if exists pos_transaction_ids;

		create temporary table pos_transaction_ids 	(
			id int primary key auto_increment,
			purchase_order_transaction_id int
			);


		while v_current_quantity > 0 do 

		    SELECT purchase_order_transaction_id,purchase_qty,purchase_rate 
		    INTO  v_purchase_order_transaction_id,v_purchase_qty,v_purchase_rate
		    FROM `pos_purchase_order_transactions_new` as purtrans  inner join pos_purchase_orders_new as purorder 
		    on purorder.purchase_order_id = purtrans.purchase_order_id and 
		    location_id = param_location_id and inventory_stock_id = param_inventory_stock_id  and 
		    purchase_order_status_id = 4 and # delivered purchase order
		    purchase_rate > 0 and  # non-adjustable purchase order transaction
	        purchase_order_transaction_id not in (select purchase_order_transaction_id from pos_transaction_ids) # not which are traversed already in reverse order
		    order by purchase_order_date desc # in reverse order 
		     limit 1;  # fetch one by one 


		    # available quantity more than the transaction 
		    if found_rows() = 0 then

		    	 # there must be at least one purchase transaction 
		    	 if exists(select * from pos_transaction_ids) then

		    	 	# find the last transaction in fifo order ,that traversed 
			    	select  purchase_order_transaction_id  into v_purchase_order_transaction_id
			    	from pos_transaction_ids  
			    	order by id desc limit 1; 

			    	# find its purchase_rate 
			    	select purchase_rate into v_purchase_rate from pos_purchase_order_transactions_new where purchase_order_transaction_id = v_purchase_order_transaction_id;	

			    else 

			    	# no purchase transaction exists for available quantity

			    	# fetch  the default quantity 
					select cost_price into v_purchase_rate from pos_location_inventory_stock where location_id = param_location_id and inventory_stock_id = param_inventory_stock_id;
			    	
		    	 end if ;


		    	 # end loop
			    set v_current_quantity = 0; 
			    	

		    else 

		    	# quantity elapse from complete of current transaction 	
		    	set v_current_quantity = v_current_quantity - v_purchase_qty;


		    	# mark transaction that it has been traversed , don't pick in next loop
			    insert into pos_transaction_ids values(null,v_purchase_order_transaction_id);

		    end if ;


		end while ;


	end if;


	# update cost price as default availabe rate in inventory 
	update pos_location_inventory_stock
	set cost_price = v_purchase_rate
	where location_id = param_location_id and inventory_stock_id = param_inventory_stock_id ;


  SET @@GLOBAL.max_sp_recursion_depth = 255;
  SET @@session.max_sp_recursion_depth = 255; 

	call updateParentInventoriesAvailableRatePerUnit(param_location_id,param_inventory_stock_id,v_purchase_rate);

end //


delimiter ;


# find all recieps where it exists , update rate and amount 
# 


drop procedure if exists updateParentInventoriesAvailableRatePerUnit;
delimiter //
create procedure updateParentInventoriesAvailableRatePerUnit(param_location_id int,param_inventory_stock_id int,param_current_rate float)
begin

	declare v_parent_inventory_stock_id int;

	declare v_finished int default 0;
	declare v_manufacture_items cursor for 
	select final_product_id
	from pos_location_inventory_stock as lis inner join pos_recipe_new as recipe
	on lis.inventory_stock_id = recipe.final_product_id
	where raw_product_id = param_inventory_stock_id  and 
	location_id = param_location_id and 	
    has_manufacturing = 1 and
    current_quantity  <= 0 ;
    declare continue handler for not found set v_finished = 1;

	open v_manufacture_items ;	

		manufacture_items: loop 

			fetch v_manufacture_items into v_parent_inventory_stock_id ;

			if v_finished = 1 then

				leave manufacture_items;

			end if;

			call updateInventoryAvailableCostPerUnit(param_location_id,v_parent_inventory_stock_id);

		end loop manufacture_items;

	close v_manufacture_items;

end //

delimiter ;

drop procedure if exists updateInventoryAvailableCostPerUnit;
delimiter //
create procedure updateInventoryAvailableCostPerUnit(param_location_id int, param_inventory_stock_id int)
begin
	
	# find all recipes , along with quantity , the iterate over to find rate and calculate cost and then finally update the cost 
	declare v_raw_product_id int;
	declare v_qty float;
	declare v_rate float;
	declare v_cost_price float default 0;

	declare v_finished int default 0;
	declare v_recipes cursor for 
	select raw_product_id,qty 
	from pos_recipe_new
	where final_product_id = param_inventory_stock_id ;
	declare continue handler for not found set v_finished = 1;


	open v_recipes ;
 
		recipes_loop : loop 

			fetch v_recipes into v_raw_product_id,v_qty;

			if v_finished = 1 then

				leave recipes_loop  ;

			end if ;

			select cost_price into v_rate
			from pos_location_inventory_stock 
			where location_id = param_location_id and inventory_stock_id = v_raw_product_id
			limit 1;

			set v_cost_price = v_cost_price + v_rate * v_qty;

		end loop recipes_loop;

	close v_recipes ;

	# update parent or manufacture cost ;
	update pos_location_inventory_stock
	set cost_price = v_cost_price
	where location_id = param_location_id and inventory_stock_id = param_inventory_stock_id;

	call updateParentInventoriesAvailableRatePerUnit(param_location_id,param_inventory_stock_id,v_cost_price);


end //


