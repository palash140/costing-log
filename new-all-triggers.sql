-- ##################################### Location Inventory  Stock Log #######################################################################3

drop trigger if exists updateLocationInventoryStockCurrentQuantity;
delimiter //
create trigger updateLocationInventoryStockCurrentQuantity
after insert on pos_location_inventory_stock_log
for each row
begin
	
	declare v_product_type int;
	declare v_is_item_in_queue int default 0;

	update pos_location_inventory_stock 
	set current_quantity = NEW.current_quantity 
	where location_id = NEW.location_id and inventory_stock_id=NEW.inventory_stock_id;

end //

delimiter ;






drop trigger if exists updateLISOnUpdateLog;
delimiter //
create trigger updateLISOnUpdateLog after update 
on pos_location_inventory_stock_log
for each row 
begin 

	update pos_location_inventory_stock 
	set current_quantity = NEW.current_quantity 
	where location_id = NEW.location_id and inventory_stock_id=NEW.inventory_stock_id;

end //

-- ##################################### Location Inventory  Stock Log #######################################################################3

delimiter ;

-- ##################################### Wastage #######################################################################3

drop trigger if exists `at_wastage_create` ;
delimiter //
create trigger `at_wastage_create`
after insert on `pos_location_inventory_stock_wastage`
for each row
begin

	declare v_last_log_id int;
	declare v_wastage_quantity float;
	

	if NEW.wastage_type_id != 4 then

		call logLocationInventoryStock(NEW.location_id,NEW.inventory_stock_id,NEW.wastage_quantity,5,NEW.wastage_id,null,NEW.wastage_date);

	    set v_last_log_id = last_insert_id();
	    set v_wastage_quantity  = NEW.wastage_quantity;




		if NEW.wastage_date < current_date() then 

			call readjustLogByDate(NEW.location_id,NEW.inventory_stock_id,NEW.wastage_date);

		end if;
		

	end if;





end //



drop trigger if exists `at_wastage_update` //
create trigger `at_wastage_update`
after update on `pos_location_inventory_stock_wastage`
for each row
begin
	

    call logLocationInventoryStock(OLD.location_id,OLD.inventory_stock_id,OLD.wastage_quantity,17,OLD.wastage_id,null,NEW.wastage_date);

    call logLocationInventoryStock(NEW.location_id,NEW.inventory_stock_id,NEW.wastage_quantity,5,NEW.wastage_id,null,NEW.wastage_date);

    if NEW.wastage_date < current_date() then 

		call readjustLogByDate(NEW.location_id,NEW.inventory_stock_id,NEW.wastage_date);

	end if;

end //


drop trigger if exists `at_wastage_delete` //
create trigger `at_wastage_delete`
after delete  on `pos_location_inventory_stock_wastage`
for each row
begin
	
    call logLocationInventoryStock(OLD.location_id,OLD.inventory_stock_id,OLD.wastage_quantity,17,OLD.wastage_id,null,OLD.wastage_date);

    if OLD.wastage_date < current_date() then 

		call readjustLogByDate(OLD.location_id,OLD.inventory_stock_id,OLD.wastage_date);

	end if;
	
end //

-- ##################################### Wastage #######################################################################3

delimiter ;

-- ##################################### Purchaes Order #######################################################################3

drop trigger if exists at_purchase_order_delivered;
delimiter // 

create trigger at_purchase_order_delivered after update
 on pos_purchase_orders_new
 for each row
 begin

 	declare v_delivered_purchase_order int default 4;
 	declare v_cancel_purchase_order int default 5;

 	if OLD.purchase_order_status_id != NEW.purchase_order_status_id then 

 		if NEW.purchase_order_status_id = v_delivered_purchase_order then 

 			call logDeliveredPurchaseOrder(NEW.location_id,NEW.purchase_order_id);

 		end if ;


 	end if ;
end //



delimiter ;

drop trigger if exists at_puruchase_order_transaction_delete;
delimiter //
create trigger at_puruchase_order_transaction_delete 
after delete on `pos_purchase_order_transactions_new`
for each row
begin 

	declare v_location_id int;
	declare v_purchase_order_date datetime;

	# operation_date also campture seconds 
	declare v_log_operation_date datetime ; 




	select location_id,purchase_order_date into v_location_id,v_purchase_order_date from pos_purchase_orders_new where purchase_order_id = OLD.purchase_order_id;

	select operation_date into v_log_operation_date 
	from pos_location_inventory_stock_log
	where location_id=v_location_id and inventory_stock_id = OLD.inventory_stock_id and operation_type_id = 2 and inventory_operation_id = OLD.purchase_order_transaction_id;
	

	# make the log entry  at same date with just after 1 second as revert 
	call logLocationInventoryStock(v_location_id,OLD.inventory_stock_id,OLD.purchase_qty,16,OLD.purchase_order_transaction_id,null,DATE_ADD(v_log_operation_date, INTERVAL 1 SECOND));

	# readjust the log
	if v_purchase_order_date < current_date() then 

		call readjustLogByDate(v_location_id,OLD.inventory_stock_id,v_purchase_order_date);

	end if;


end //

delimiter ;


-- drop trigger if exists atBeforePurchaseOrderTransactions;
-- delimiter //
-- create trigger atBeforePurchaseOrderTransactions
-- before insert on 'pos_purchase_order_transactions_new'
-- for each row
-- begin

-- 	set NEW.available_quantity = NEW.purchase_qty ;

-- end // 
-- delimiter ;




drop trigger if exists at_puruchase_order_transaction_update;
delimiter //
create trigger at_puruchase_order_transaction_update 
after update on `pos_purchase_order_transactions_new`
for each row
begin 


	declare v_last_purchase_qty float;
	declare v_location_id int;
	declare v_purhcase_order_date datetime;


	if OLD.purchase_qty != NEW.purchase_qty then
		-- revert last log of this purchase	

		select location_id,purchase_order_date into v_location_id,v_purhcase_order_date from pos_purchase_orders_new where purchase_order_id = NEW.purchase_order_id;

		-- last qty minus
		call logLocationInventoryStock(v_location_id,NEW.inventory_stock_id,OLD.purchase_qty,16,NEW.purchase_order_transaction_id,null,v_purhcase_order_date);

		-- update to new qty 
		call logLocationInventoryStock(v_location_id,NEW.inventory_stock_id,NEW.purchase_qty,2,NEW.purchase_order_transaction_id,null,v_purhcase_order_date);


		if v_purhcase_order_date < current_date() then 

			call readjustLogByDate(v_location_id,NEW.inventory_stock_id,v_purhcase_order_date);

		end if;


	end if;	

end //

-- ##################################### Purchaes Order #######################################################################3

delimiter ;


-- ####################################### Manufacture #############################################

drop trigger if exists logUpdateManufactureStock;
delimiter //
create trigger logUpdateManufactureStock
after update on pos_manufacture 
for each row
begin

	declare v_log_id int;
	declare v_parent_id int default null;
	
	
	IF NEW.manufacturing_quantity != OLD.manufacturing_quantity  then

 		-- 12  manufactur revert minus
 		-- 10 sale manufacture revert
	 	call logLocationInventoryStock(OLD.location_id,OLD.inventory_stock_id,OLD.manufacturing_quantity,
	 	IF(OLD.manufacture_type_id = 1,12,10),
	 	OLD.manufacture_id,null,NEW.manufacturing_date,v_parent_id,v_log_id);

	 	-- 13 manufacture consumptions revert plus
	 	-- 11 sale manufacture consumptions revert
	 	call logItemRecipes(OLD.location_id,OLD.inventory_stock_id,OLD.manufacturing_quantity,
	 	IF(OLD.manufacture_type_id = 1,13,11),
	 	OLD.manufacture_id,NEW.manufacturing_date,v_log_id);


	 	-- 4 manufacture plus
	 	-- 7 sale manufacture plus
	 	call logLocationInventoryStock(NEW.location_id,NEW.inventory_stock_id,NEW.manufacturing_quantity,
	 	IF(OLD.manufacture_type_id = 1,4,7),
	 	NEW.manufacture_id,null,NEW.manufacturing_date);



	 	-- 6 manufacture consumptions minus
	 	-- 8 sales manufacture consumptions
	 	call logItemRecipes(NEW.location_id,NEW.inventory_stock_id,NEW.manufacturing_quantity,
	 	IF(OLD.manufacture_type_id = 1,6,8),
	 	NEW.manufacture_id,NEW.manufacturing_date);

	end if ;


end //

delimiter ;



drop trigger if exists logLocationInventoryStockManufacture;
delimiter //
CREATE TRIGGER `logLocationInventoryStockManufacture` AFTER INSERT ON `pos_manufacture`
 FOR EACH ROW 
 begin


	declare v_costing_tree_node_type_id int default 4;
	declare v_parent_id int default null;
	declare v_costing_tree_node_id int;
	declare v_costing float default null;
	declare v_in_queue int default 0;

	# 2 means manufacture type
	declare v_log_id int;

	declare v_normal_manufacture int default 4;
	declare v_sale_manufacture int default 7;
	declare v_normal_manufacture_consumptions int default 6;
	declare v_sales_manufacture_consumptions int default 8;

	declare v_operation_type int; 
	declare v_other_info text default null;
	declare v_recipe_type int;

	declare v_costing_quantity float;


	if NEW.manufacture_type_id = 1 then

		set v_operation_type = v_normal_manufacture;
		set v_recipe_type = v_normal_manufacture_consumptions;

	else

		set v_operation_type = v_sale_manufacture;
		set v_recipe_type = v_sales_manufacture_consumptions;

	end if;


	call logLocationInventoryStock(NEW.manufacturing_date,NEW.location_id,NEW.inventory_stock_id,v_operation_type,NEW.manufacture_id,NEW.manufacturing_quantity,v_other_info,v_log_id);

 	select createInventoryCostingTreeNode(v_log_id,NEW.manufacturing_quantity,v_costing_tree_node_type_id,v_parent_id) into v_costing_tree_node_id;

 	call logManufactureInventoryRecipes(v_recipe_type,NEW.manufacturing_date,NEW.location_id,NEW.inventory_stock_id,v_operation_type,NEW.manufacture_id,NEW.manufacturing_quantity,v_log_id,v_costing_tree_node_id);

 	-- 6 manufacture consumptions minus
 	-- 8 sales manufacture consumption
# 	call logItemRecipes(NEW.location_id,NEW.inventory_stock_id,NEW.manufacturing_quantity,
# 	IF(NEW.manufacture_type_id = 1 ,6,8),
# 	NEW.manufacture_id,NEW.manufacturing_date,v_costing_tree_node_id);


  end   //
  
delimiter ;




drop trigger if exists logDeleteManufactureStock;
delimiter //
create trigger logDeleteManufactureStock
before delete on pos_manufacture 
for each row
begin 

	-- 12  manufactur revert minus
	-- 10 sale manufacture revert minus
 	call logLocationInventoryStock(OLD.location_id,OLD.inventory_stock_id,OLD.manufacturing_quantity,
 	IF(OLD.manufacture_type_id = 1,12,10),
 	OLD.manufacture_id,null,OLD.manufacturing_date);


 	-- 13 manufacture consumptions revert plus
 	-- 11 sale manufacture consumptions revert plus
 	call logItemRecipes(OLD.location_id,OLD.inventory_stock_id,OLD.manufacturing_quantity,
 	IF(OLD.manufacture_type_id = 1,13,11),
 	OLD.manufacture_id,OLD.manufacturing_date);


end // 


-- ############################################# Manufacture #######################################

delimiter ;


-- ############################################# Sales Order #######################################

drop trigger if exists AtSalesOrderUpdate;
delimiter //
CREATE trigger AtSalesOrderUpdate
after update on `pos_sales_order`
for each row 
begin 
	
  	IF OLD.sales_order_status_id!=NEW.sales_order_status_id then
	
		if   NEW.sales_order_status_id = 5 then

			call cancelSalesOrder(NEW.sales_order_id,NEW.location_id,NEW.company_id,NEW.floor_id,NEW.employee_id);

         end if;


         -- sales order status changed to ordered 
    	if NEW.sales_order_status_id = 4  THEN

    		CALL logDeliveredSalesOrderTransactions(NEW.location_id,NEW.sales_order_id,NEW.sales_order_type_id);

    	end if;


    	if OLD.sales_order_status_id = 4 and NEW.sales_order_status_id = 2 then

	  		call logDeliverToProgressSalesOrderTransactions(NEW.location_id,NEW.sales_order_id,NEW.sales_order_type_id);
  	
		 end if;

	  
	END IF;

end //

delimiter ;

--- ########################################### costing ##############################3
drop trigger if exists calculateLogItemCosting;
delimiter //
create trigger calculateLogItemCosting
after insert on pos_log_item_costing_transactions
for each row 
begin

	
	declare v_log_quantity float;
	declare v_log_costing_quantity float;
	declare v_costing float;

	# get quantity operation 	
	select quantity into v_log_quantity from pos_location_inventory_stock_log where log_id = NEW.log_id;


	# sum of its costing quantity 
	select sum(quantity),sum(cost) into v_log_costing_quantity,v_costing
	from pos_log_item_costing_transactions where log_id = NEW.log_id;


	if v_log_quantity = v_log_costing_quantity then 

		update pos_location_inventory_stock_log
		set costing = v_costing
		where log_id = NEW.log_id;

	end if


	# also update the available quantity of transactions 
	update pos_purchase_order_transactions_new
	set available_quantity = available_quantity - NEW.quantity 
	where purchase_order_transaction_id = NEW.purchase_order_transaction_id;

end //










